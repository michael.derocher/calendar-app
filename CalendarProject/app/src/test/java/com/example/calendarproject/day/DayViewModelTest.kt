//package com.example.calendarproject.day
//
//import android.app.Application
//import androidx.activity.compose.setContent
//import androidx.compose.ui.platform.LocalContext
//import androidx.compose.ui.res.stringResource
//import androidx.navigation.NavHostController
//import androidx.navigation.compose.rememberNavController
//import com.example.calendarproject.Domain.Classes.Event
//import com.example.calendarproject.Domain.NavRoutes
//import com.example.calendarproject.Presentation.MainActivity
//import com.example.calendarproject.Presentation.NavigationHost
//import com.example.calendarproject.Presentation.ViewModels.DayViewModel
//import com.example.calendarproject.Presentation.ViewModels.EventViewModel
//import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
//import com.example.calendarproject.R
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//import org.junit.jupiter.api.Assertions.*
//import java.time.LocalDate
//
//
//class DayViewModelTest {
//    @Test
//    fun previousDay() {
//        val viewModel = DayViewModel(LocalDate.of(2023, 1, 1), ArrayList<Event>())
//
//        viewModel.previousDay()
//
//        assertEquals(31, viewModel.currentDay)
//        assertEquals("December", viewModel.currentMonth)
//        assertEquals(2022, viewModel.currentYear)
//        assertEquals("Saturday", viewModel.currentWeekday)
//    }
//
//    @Test
//    fun nextDay() {
//        val viewModel = DayViewModel(LocalDate.of(2022, 12, 31), ArrayList<Event>())
//
//        viewModel.nextDay()
//
//        assertEquals(1, viewModel.currentDay)
//        assertEquals("January", viewModel.currentMonth)
//        assertEquals(2023, viewModel.currentYear)
//        assertEquals("Sunday", viewModel.currentWeekday)
//    }
//
//    @Test
//    fun parseTime() {
//        val viewModel = DayViewModel(LocalDate.of(2023, 1, 1), ArrayList<Event>())
//
//        val result = viewModel.parseTime(13.5)
//
//        assertEquals("13:30", result)
//    }
//
//    @Test fun displayDayText(){
//        val viewModel = DayViewModel(LocalDate.of(2023, 1, 1), ArrayList<Event>())
//
//        val result1 = viewModel.displayDayText(1)
//        val result2 = viewModel.displayDayText(22)
//        val result3 = viewModel.displayDayText(23)
//        val result4 = viewModel.displayDayText(11)
//
//        assertEquals("1st", result1)
//        assertEquals("22nd", result2)
//        assertEquals("23rd", result3)
//        assertEquals("11th", result4)
//    }
//}