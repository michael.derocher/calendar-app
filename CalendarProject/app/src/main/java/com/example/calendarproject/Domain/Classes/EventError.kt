package com.example.calendarproject.Domain.Classes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class EventError(val display: Boolean, val title : String, val description : String) : Parcelable