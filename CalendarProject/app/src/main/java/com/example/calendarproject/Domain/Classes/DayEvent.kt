package com.example.calendarproject.Domain.Classes

data class DayEvent(
    val title: String,
    val startTime: String,
    val endTime: String,
    val event: Event
){
    val duration = processDuration(endTime, startTime)

    fun processDuration(endTime: String, startTime: String): Double{
        val endTimeWhole = endTime.split(":")[0].toDouble()
        val endTimeFraction = (endTime.split(":")[1].toDouble()) / 60.0
        val startTimeWhole = startTime.split(":")[0].toDouble()
        val startTimeFraction = (startTime.split(":")[1].toDouble()) / 60.0
        return (endTimeWhole + endTimeFraction) - (startTimeWhole + startTimeFraction)
    }
}
