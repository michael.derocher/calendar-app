package com.example.calendarproject.Presentation.ViewModels

import android.app.Application
import android.annotation.SuppressLint
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.calendarproject.Data.EventRepository
import com.example.calendarproject.Data.EventRoomDatabase
import com.example.calendarproject.Domain.Classes.Event
import java.lang.Integer.parseInt
import java.util.Calendar
import java.util.Calendar.DAY_OF_MONTH
import java.util.Calendar.MONTH
import java.util.Calendar.YEAR
import java.util.Locale

@SuppressLint("MutableCollectionMutableState")
class EventViewModel(application: Application) : ViewModel() {
    var currentEvent: Event = Event()
    var isEventSet by mutableStateOf(false)

    private val repository : EventRepository
    private val eventsInCurrentDay: MutableLiveData<List<Event>>

    var calendar = Calendar.getInstance()
    var month: String = (calendar.get(MONTH) + 1).toString()
    var day: String = calendar.get(DAY_OF_MONTH).toString()
    var year: String = calendar.get(YEAR).toString()

    init {
        val eventDb = EventRoomDatabase.getInstance(application)
        val eventDao = eventDb.eventDao()
        repository = EventRepository(eventDao)

        eventsInCurrentDay = repository.eventsInCurrentDay
    }

    fun addEvent(event: Event){
        repository.insertEvent(event)
    }

    fun confirmEdit(){
        var dateParts = date.split("/")
        var formattedDate = "${dateParts[0]}/${dateParts[1]}/${dateParts[2]}"
        if (Locale.getDefault().language.equals("fr")){
            formattedDate= "${dateParts[1]}/${dateParts[0]}/${dateParts[2]}"
        }
        currentEvent.title = title
        currentEvent.subject = subject
        currentEvent.date = formattedDate
        currentEvent.startTime = startTime
        currentEvent.endTime = endTime
        currentEvent.location = location
        currentEvent.description = description

        repository.updateEvent(currentEvent)
    }

    fun deleteEvent(eventId: Int){
        repository.deleteEvent(eventId)
    }
    fun createEvent(){
        var dateParts = date.split("/")
        var formattedDate = "${dateParts[0]}/${dateParts[1]}/${dateParts[2]}"
        if (Locale.getDefault().language.equals("fr")){
            formattedDate= "${dateParts[1]}/${dateParts[0]}/${dateParts[2]}"
        }
        val newEvent = Event(
            title,
            subject,
            formattedDate,
            startTime,
            endTime,
            location,
            description
        )

        repository.insertEvent(newEvent)
        newEvent.id = repository.getNewEventId()
        setEvent(newEvent)
    }

    fun setEvent(event: Event) {
        this.currentEvent = event
        var dateFormatted = this.currentEvent.date
        if (dateFormatted.isNotEmpty() && Locale.getDefault().language.equals("fr")){
            val dateParts = dateFormatted.split("/")
            dateFormatted = "${dateParts[1]}/${dateParts[0]}/${dateParts[2]}"
        }
        onTitleChange(this.currentEvent.title)
        onSubjectChange(this.currentEvent.subject)
        onDateChange(dateFormatted)
        onStartTimeChange(this.currentEvent.startTime)
        onEndTimeChange(this.currentEvent.endTime)
        onLocationChange(this.currentEvent.location)
        onDescriptionChange(this.currentEvent.description)
    }
    fun toggleEventSet(){
        this.isEventSet = !this.isEventSet
    }

    var title by mutableStateOf("")
    fun onTitleChange(newTitle: String) {
        title = newTitle
    }

    var subject by mutableStateOf("")
    fun onSubjectChange(newSubject: String) {
        subject = newSubject
    }

    var date by mutableStateOf(setUpInitialDate())
    fun onDateChange(newDate: String) {
        date = newDate
    }
    fun setUpInitialDate(): String{
        if (Locale.getDefault().language.equals("en")){
            return "${month.padStart(2, '0')}/${day.padStart(2, '0')}/${year}"
        }
        return "${day.padStart(2, '0')}/${month.padStart(2, '0')}/${year}"
    }

    var startTime by mutableStateOf("10:00")
    fun onStartTimeChange(newStartTime: String) {
        if (endTime != "" &&
            timeStringToMinutes(newStartTime) >= timeStringToMinutes(endTime)) {
            endTime = ""
        }

        startTime = newStartTime
    }

    var endTime by mutableStateOf("11:00")
    fun onEndTimeChange(newEndTime: String) {
        if (timeStringToMinutes(newEndTime) <= timeStringToMinutes(startTime)){
            startTime = ""
        }

        endTime = newEndTime
    }

    private fun timeStringToMinutes(time: String) : Int {
        if (time == "") {
            return 0
        }
        val values = time.split(":")
        return (parseInt(values[0]) * 60 + parseInt(values[1]))
    }

    var location by mutableStateOf("")
    fun onLocationChange(newLocation: String) {
        location = newLocation
    }

    var description by mutableStateOf("")
    fun onDescriptionChange(newDescription: String) {
        description = newDescription
    }

    fun resetNewEvent(){
        onTitleChange("")
        onSubjectChange("")
        onDateChange(setUpInitialDate())
        onStartTimeChange("")
        onEndTimeChange("")
        onLocationChange("")
        onDescriptionChange("")
    }

    fun containsEmptyFields() : Boolean {
        return  title == "" ||
                subject == "" ||
                date == "" ||
                startTime == "" ||
                endTime == ""
    }

    fun updateEventsInCurrentDay() {
        if (date.isNotEmpty()){
            var dateParts = date.split("/")
            var formattedDate = "${dateParts[0]}/${dateParts[1]}/${dateParts[2]}"
            if (Locale.getDefault().language.equals("fr")){
                formattedDate= "${dateParts[1]}/${dateParts[0]}/${dateParts[2]}"
            }
            repository.getDateEvents(formattedDate)
        }
    }

    fun hasOverlappingEvents(): Boolean {
        for (event in eventsInCurrentDay.value!!) {
            if (event.id != currentEvent.id && eventsOverlap(event.startTime, event.endTime, startTime, endTime)) {
                return true
            }
        }

        return false
    }


    private fun eventsOverlap(start1 : String, end1 : String, start2 : String, end2 : String) : Boolean {
        val start1 = timeStringToMinutes(start1)
        val start2 = timeStringToMinutes(start2)

        val end1 = timeStringToMinutes(end1)
        val end2 = timeStringToMinutes(end2)

        // Case 1: Schedule 1 starts before Schedule 2, and ends during Schedule 2
        if (start1 < start2 && end1 > start2 && (end1 < end2 || end1 == end2)) {
            return true
        }

        // Case 2: Schedule 1 starts before Schedule 2, and ends after Schedule 2
        if ((start1 < start2 || start1 == start2) && end1 > end2) {
            return true
        }

        // Case 3: Schedule 1 starts during Schedule 2, and ends during Schedule 2
        if ((start1 > start2 || start1 == start2) && (end1 < end2 || end1 == end2)) {
            return true
        }

        // Case 4: Schedule 1 starts during Schedule 2, and ends after Schedule 2
        if ((start1 > start2 || start1 == start2) && start1 < end2 && end1 > end2) {
            return true
        }

        return false
    }
}