package com.example.calendarproject.Data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.calendarproject.Domain.Classes.Event

@Dao
interface EventDao {
    @Insert
    suspend fun insertEvent(event: Event)
    @Query("SELECT * FROM events")
    fun getAllEvents(): LiveData<List<Event>>

    @Query("SELECT * FROM events WHERE date LIKE :month || '/%' AND date LIKE '%/' || :year")
    fun getMonthEvents(month: String, year: String): List<Event>

    @Query("SELECT * FROM events WHERE date LIKE '%/' || :month || '/%'")
    fun getMonthEventsFr(month: String): List<Event>

    @Query("SELECT * FROM events WHERE date = :date")
    fun getDateEventsLive(date: String): LiveData<List<Event>>

    @Query("SELECT * FROM events WHERE date = :date")
    fun getDateEvents(date: String): List<Event>

    @Query("SELECT * FROM events WHERE id = :id")
    fun getEvent(id: Int): LiveData<Event>

    @Query("SELECT MAX(id) FROM events")
    suspend fun getNewEventId(): Int

    @Query("DELETE FROM events WHERE id = :id")
    fun deleteEvent(id: Int)

    @Query("UPDATE events SET title = :title, " +
            "subject = :subject, " +
            "date = :date, " +
            "startTime = :startTime, " +
            "endTime = :endTime, " +
            "location = :location, " +
            "description = :description " +
            "WHERE id = :id")
    fun updateEvent(title: String, subject: String, date: String, startTime: String, endTime: String,
                    location: String, description: String, id: Int)
}