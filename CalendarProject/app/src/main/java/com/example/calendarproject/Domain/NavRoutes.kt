package com.example.calendarproject.Domain

sealed class NavRoutes (val route: String) {
    object Month : NavRoutes("Month Overview")
    object Day : NavRoutes("Day Overview")
    object View : NavRoutes("View Event")
    object Add : NavRoutes("Add Event")
    object Edit : NavRoutes("Edit Event")
    object Weather : NavRoutes("Weather Overview")
}
