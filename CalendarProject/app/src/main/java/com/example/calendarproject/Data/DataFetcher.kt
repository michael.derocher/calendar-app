package com.example.calendarproject.Data

import android.util.Log
import java.net.HttpURLConnection
import java.net.URL

class DataFetcher {
    fun fetch(url: String) : String {
        try{
            val url = URL(url)
            val httpURLConnection = url.openConnection() as HttpURLConnection

            httpURLConnection.requestMethod = "GET"
            httpURLConnection.setRequestProperty("Accept", "text/json")

            val responseCode = httpURLConnection.responseCode
            Log.d("response code", responseCode.toString())

            return if (responseCode == HttpURLConnection.HTTP_OK) {
                httpURLConnection.inputStream.bufferedReader()
                    .use { it.readText() }

            } else {
                ""
            }
        } catch (exception : Exception) {
            return ""
        }
    }
}