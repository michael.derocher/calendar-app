package com.example.calendarproject.Presentation.ViewModels

import android.app.Application
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.calendarproject.Data.EventRepository
import com.example.calendarproject.Data.EventRoomDatabase
import com.example.calendarproject.Data.HolidayRepository
import com.example.calendarproject.Domain.Classes.DayEvent
import com.example.calendarproject.Domain.Classes.HolidayResponse
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.util.Calendar
import java.util.Locale

class DayViewModel(date: LocalDate, application: Application, vmStrings: Array<String>) : ViewModel() {
    private val strings = vmStrings
    private val calendar = Calendar.getInstance()
    var currentYear by mutableStateOf(2023)
    var currentMonth by mutableStateOf(strings[0])
    var currentDay by mutableStateOf(1)
    var currentWeekday by mutableStateOf(strings[13])
    private val eventRepository : EventRepository
    val events: MutableLiveData<List<DayEvent>>

    private var lastFetchedYear : String? = null
    private var lastFetchedCountryCode : String? = null
    private val holidayRepository = HolidayRepository()
    private val _yearlyHolidays = MutableLiveData<List<HolidayResponse>>()
    val yearlyHolidays : LiveData<List<HolidayResponse>> = _yearlyHolidays
    var dailyHolidays : List<HolidayResponse> = listOf<HolidayResponse>()

    var country_code = ""

    init {
        setDate(date)
        val eventDb = EventRoomDatabase.getInstance(application)
        val eventDao = eventDb.eventDao()
        eventRepository = EventRepository(eventDao)

        updateDateEvents()
        events = eventRepository.dailyEvents
    }

    fun fetchYearHolidays(year : String, country_code : String){
        if(year != lastFetchedYear || country_code != lastFetchedCountryCode) {
            lastFetchedYear = year
            lastFetchedCountryCode = country_code
            viewModelScope.launch {
                try {
                    val holidays = holidayRepository.getHolidays(year, country_code)
                    _yearlyHolidays.value = holidays
                    Log.e("FetchHolidays", _yearlyHolidays.value.toString())
                } catch (e: Exception) {
                    // Handle error
                    Log.e("FetchHolidays", e.message.toString())
                }
            }
        }
    }

    fun updateDailyHolidays(){
        val filterDailyHolidays = ArrayList<HolidayResponse>()
        if(yearlyHolidays.value != null) {
            for (holiday in yearlyHolidays.value!!) {
                val splitDate = holiday.date.split("-")
                if (splitDate[1] == (calendar.get(Calendar.MONTH) + 1).toString().padStart(
                        2,
                        '0'
                    ) && splitDate[2] == (calendar.get(Calendar.DAY_OF_MONTH)).toString()
                        .padStart(2, '0')
                ) {
                    filterDailyHolidays.add(holiday)
                }
            }
        }
        dailyHolidays = filterDailyHolidays.toList()
    }

    fun setDate(date: LocalDate){
        calendar.set(date.year, date.monthValue - 1, date.dayOfMonth)
        currentYear = calendar.get(Calendar.YEAR)
        currentMonth = parseMonth(calendar.get(Calendar.MONTH))
        currentDay = calendar.get(Calendar.DAY_OF_MONTH)
        currentWeekday = parseWeekday(calendar.get(Calendar.DAY_OF_WEEK))
    }

    fun updateDateEvents(){
        eventRepository.getDateEvents(getCalendarDateStringDb())
    }

    fun getCalendarDateStringDb(): String{
        var dayFormatted = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(2, '0')
        var monthFormatted = (calendar.get(Calendar.MONTH) + 1).toString().padStart(2, '0')
        return "${monthFormatted}/$dayFormatted/${calendar.get(Calendar.YEAR)}"
    }

    fun getCalendarDateString() : String{
        var dayFormatted = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(2, '0')
        var monthFormatted = (calendar.get(Calendar.MONTH) + 1).toString().padStart(2, '0')
        if (Locale.getDefault().language.equals("en")){
            return "${monthFormatted}/$dayFormatted/${calendar.get(Calendar.YEAR)}"
        }
        return "$dayFormatted/${monthFormatted}/${calendar.get(Calendar.YEAR)}"
    }

    fun previousDay(){
        calendar.add(Calendar.DAY_OF_YEAR, -1)
        currentYear = calendar.get(Calendar.YEAR)
        currentMonth = parseMonth(calendar.get(Calendar.MONTH))
        currentDay = calendar.get(Calendar.DAY_OF_MONTH)
        currentWeekday = parseWeekday(calendar.get(Calendar.DAY_OF_WEEK))
        updateDateEvents()
        fetchYearHolidays(currentYear.toString(), country_code)
        updateDailyHolidays()
    }

    fun nextDay(){
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        currentYear = calendar.get(Calendar.YEAR)
        currentMonth = parseMonth(calendar.get(Calendar.MONTH))
        currentDay = calendar.get(Calendar.DAY_OF_MONTH)
        currentWeekday = parseWeekday(calendar.get(Calendar.DAY_OF_WEEK))
        updateDateEvents()
        fetchYearHolidays(currentYear.toString(), country_code)
        updateDailyHolidays()
    }

    fun parseTime(hours : Double): String{
        val minutes = (hours*60).toInt()
        return if((minutes/60) == 0){
            "12:${parseMinutes((minutes%60).toString())}"
        } else{
            "${(minutes/60)}:${parseMinutes((minutes%60).toString())}"
        }
    }

    private fun parseMinutes(minutes : String) : String {
        if(minutes.length == 1){
            return "${minutes}0"
        }
        return minutes
    }

    fun displayDayText(day : Int) : String{
        if (Locale.getDefault().language.equals("en")){
            return if(day in 11..13){
                "${day}th"
            } else if(day%10 == 1){
                "${day}st"
            } else if (day%10 == 2){
                "${day}nd"
            } else if (day%10 == 3){
                "${day}rd"
            } else {
                "${day}th"
            }
        }
        return "${day}e"
    }

    private fun parseWeekday(weekday : Int) : String {
        return when(weekday){
            1 -> strings[12]
            2 -> strings[13]
            3 -> strings[14]
            4 -> strings[15]
            5 -> strings[16]
            6 -> strings[17]
            else -> strings[18]
        }
    }
    fun parseMonth(month : Int): String{
        return when(month){
            0 -> strings[0]
            1 -> strings[1]
            2 -> strings[2]
            3 -> strings[3]
            4 -> strings[4]
            5 -> strings[5]
            6 -> strings[6]
            7 -> strings[7]
            8 -> strings[8]
            9 -> strings[9]
            10 -> strings[10]
            else -> strings[11]
        }
    }

    fun getCurrentMonthIndex() : Int {
        return calendar.get(Calendar.MONTH)
    }
}