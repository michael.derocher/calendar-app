package com.example.calendarproject.Presentation.screens.Event

import android.app.AlertDialog
import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.R
import java.util.Calendar
import java.util.Locale

@Composable
fun ManipulateEvent(navController: NavController,
                    context: Context = LocalContext.current,
                    calendar: Calendar,
                    decision: @Composable (navController: NavController, viewModel: EventViewModel) -> Unit,
                    viewModel: EventViewModel,
                    monthViewModel: MonthViewModel,
                    displayDate: Boolean,
                    specifier: String) {
    val datePicker = DateSelection(context, monthViewModel) {
        if (Locale.getDefault().language.equals("en")){
            val splitDate = it.split('/')
            viewModel.onDateChange("${splitDate[1]}/${splitDate[0]}/${splitDate[2]}")
        }
        else{
            viewModel.onDateChange(it)
        }
        viewModel.updateEventsInCurrentDay()
    }

    datePicker.datePicker.minDate = calendar.timeInMillis

    val startTimePicker = TimeSelection(context, calendar) {
        viewModel.onStartTimeChange(it)
    }

    val endTimePicker = TimeSelection(context, calendar) {
        viewModel.onEndTimeChange(it)
    }

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .fillMaxHeight()
            .height(IntrinsicSize.Max)
            .testTag("topColumn")
    ) {
        InputFields(viewModel, datePicker, startTimePicker, endTimePicker, displayDate, specifier)
        decision(navController, viewModel)
    }
}

@Composable
fun InputFields(viewModel: EventViewModel,
                datePicker : AlertDialog,
                startTimePicker : AlertDialog,
                endTimePicker : AlertDialog,
                displayDate: Boolean,
                specifier: String) {
    Title("$specifier ${stringResource(R.string.viewfield_title)}")
    GeneralField("${stringResource(R.string.title)}:", viewModel.title, stringResource(R.string.enter_title)) {
        viewModel.onTitleChange(it)
    }
    GeneralField("${stringResource(R.string.viewfield_subject)}:", viewModel.subject, stringResource(R.string.enter_subject)) {
        viewModel.onSubjectChange(it)
    }
    if (displayDate) {
        DateTimeField("${stringResource(R.string.viewfield_date)}:", datePicker, viewModel.date, stringResource(R.string.enter_date))
    }
    DateTimeField("${stringResource(R.string.viewfield_start)}:", startTimePicker, viewModel.startTime, stringResource(R.string.enter_start_time))
    DateTimeField("${stringResource(R.string.viewfield_end)}:", endTimePicker, viewModel.endTime, stringResource(R.string.enter_end_time))
    GeneralField("${stringResource(R.string.viewfield_location)}:", viewModel.location, stringResource(R.string.enter_location)) {
        viewModel.onLocationChange(it)
    }
    DescriptionField(viewModel.description, stringResource(R.string.enter_description)) {
        viewModel.onDescriptionChange(it)
    }
}