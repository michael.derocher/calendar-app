package com.example.calendarproject.Presentation.screens.Month

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.navigation.NavController
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Button
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTagsAsResourceId
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.R
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import java.time.LocalDate
import java.util.Calendar

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun Month(navController : NavController, viewModel: MonthViewModel, dayViewModel : DayViewModel, eventViewModel: EventViewModel) {
    Scaffold(
        modifier = Modifier
            .semantics { testTagsAsResourceId = true }
            .testTag("MonthScaffold"),
        topBar = {
            CenterAlignedTopAppBar(
                title = { Title(viewModel) },
                navigationIcon = { PreviousMonthButton(viewModel) },
                actions = { NextMonthButton(viewModel) }
            )
        },
        content = { innerPadding ->
            val daysContainingEvents by viewModel.daysContainingEvents.observeAsState(listOf())
            Content(innerPadding, viewModel, navController, daysContainingEvents, dayViewModel, eventViewModel)
        }
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Title(viewModel: MonthViewModel) {
    Text(
        text = viewModel.currentMonthScreenDate,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier
            .semantics { testTagsAsResourceId = true }
            .testTag("Title")
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun PreviousMonthButton(viewModel: MonthViewModel) {
    IconButton(
        onClick = { viewModel.previousMonth() },
        modifier = Modifier
            .semantics { testTagsAsResourceId = true }
            .testTag("Previous")
    ) {
        Icon(
            imageVector = Icons.Filled.ArrowBack,
            contentDescription = stringResource(R.string.previous_month)
        )
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun NextMonthButton(viewModel: MonthViewModel) {
    IconButton(
        onClick = { viewModel.nextMonth() },
        modifier = Modifier
            .semantics { testTagsAsResourceId = true }
            .testTag("Next")
    ) {
        Icon(
            imageVector = Icons.Default.ArrowForward,
            contentDescription = stringResource(R.string.next_month)
        )
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Content(innerPadding : PaddingValues, viewModel: MonthViewModel, navController : NavController, daysContainingEvents : List<Int>, dayViewModel : DayViewModel, eventViewModel: EventViewModel) {
    Column(
        modifier = Modifier
            .padding(innerPadding)
            .fillMaxSize()
            .semantics { testTagsAsResourceId = true }
            .testTag("ContentColumn")
        ,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        AddEventButton(navController, eventViewModel)
        DaysOfTheWeek(viewModel)
        DaysOfMonth(viewModel, navController, daysContainingEvents, dayViewModel)
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddEventButton(navController : NavController, eventViewModel: EventViewModel) {
    Row (
        modifier = Modifier
            .fillMaxWidth()
            .semantics { testTagsAsResourceId = true }
            .testTag("AddEventRow"),
        horizontalArrangement = Arrangement.Center
    ){
        Button(
            modifier = Modifier
                .padding(end = 10.dp)
                .semantics { testTagsAsResourceId = true }
                .testTag("addEventButton"),
            onClick = {
                eventViewModel.setEvent(Event("", "", "", "", "", "", ""))
                navController.navigate(NavRoutes.Add.route + "/month")
            }
        ) {
            Text(text = stringResource(R.string.month_add_event))
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DaysOfTheWeek(viewModel : MonthViewModel) {
    val daysOfTheWeek = viewModel.daysOfWeek

    Row (
        modifier = Modifier
            .padding(start = 10.dp, end = 10.dp)
            .fillMaxWidth()
            .semantics { testTagsAsResourceId = true }
            .testTag("DaysOfWeekRow"),
        horizontalArrangement = Arrangement.spacedBy(5.dp)
    ){
        for (day in daysOfTheWeek) {
            val modifier = Modifier
                .background(Color.Blue)
                .weight(1f)

            Day(day, modifier)
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun Day(text : String, modifier: Modifier) {
    Box(modifier = modifier
        .semantics { testTagsAsResourceId = true }
        .testTag("Day")) {
        Text(text = text, color = Color.White, modifier = Modifier.align(Alignment.Center))
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun NonClickableDay() {
    TextButton(onClick = {}, modifier = Modifier
        .background(Color.White)
        .semantics { testTagsAsResourceId = true }
        .testTag("ClickableDay")) {
        Text(text = "", color = Color.White)
    }
}
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ClickableDay(text : String, monthViewModel: MonthViewModel, color : Color, onClick : () -> Unit) {
    TextButton(onClick = onClick, modifier = Modifier
        .background(color)
        .semantics { testTagsAsResourceId = true }
        .testTag("ClickableDay")) {
        DayText(text, monthViewModel)
    }
}

@Composable
fun DayText(text: String, monthViewModel: MonthViewModel){
    if(isCurrentDate(text, monthViewModel)){
        Text(text = text,
            color = Color(255, 222, 26),
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            textDecoration = TextDecoration.Underline)
    }
    else{
        Text(text = text, color = Color. White)
    }
}

fun isCurrentDate (text: String, monthViewModel: MonthViewModel): Boolean{
    val currentDate = Calendar.getInstance()
    if (currentDate.get(Calendar.YEAR) == monthViewModel.calendar.get(Calendar.YEAR)
        && currentDate.get(Calendar.MONTH) == monthViewModel.calendar.get(Calendar.MONTH)
        && text.toInt() == currentDate.get(Calendar.DAY_OF_MONTH)){
        return true
    }
    return false
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DaysOfMonth(viewModel: MonthViewModel, navController: NavController, daysContainingEvents : List<Int>, dayViewModel : DayViewModel) {
    val dayOfWeek = viewModel.getStartingDayOfWeek()
    val daysInMonth = viewModel.daysInCurrentMonth

    LazyVerticalGrid(
        columns = GridCells.Fixed(7),
        verticalArrangement = Arrangement.spacedBy(5.dp),
        horizontalArrangement = Arrangement.spacedBy(5.dp),
        contentPadding = PaddingValues(
            start = 10.dp,
            end = 10.dp
        ),
        content = {
            items(dayOfWeek) { _ ->
                NonClickableDay()
            }
            items(daysInMonth) { index ->
                val dayContainsEvent = daysContainingEvents.contains(index+1)
                val dayColor = if (dayContainsEvent) Color.Magenta else Color.Blue

                ClickableDay("${index+1}", viewModel, dayColor) {
                    viewModel.selectedDate = LocalDate.of(viewModel.calendar.get(Calendar.YEAR), viewModel.calendar.get(Calendar.MONTH) + 1, index + 1)
                    dayViewModel.setDate(viewModel.selectedDate)
                    navController.navigate(NavRoutes.Day.route + "/month")
                }
            }
        }, modifier = Modifier
            .semantics { testTagsAsResourceId = true }
            .testTag("DaysOfMonthGrid")
    )
}