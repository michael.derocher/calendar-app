package com.example.calendarproject.Presentation.ViewModels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.calendarproject.Data.DataFetcher
import com.example.calendarproject.Domain.Classes.WeatherData
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

enum class Tab {
    OVERVIEW,
    PRECIPITATION,
    WIND,
    HUMIDITY
}
class WeatherViewModel : ViewModel() {
    private val dataFetcher = DataFetcher()
    private val url = "https://api.weatherapi.com/v1/forecast.json?key=f753a508d9cc4c9c8d601920232012&days=5&q="

    val selectedTab = mutableStateOf(Tab.OVERVIEW)
    val selectedDayIndex = mutableIntStateOf(0)
    val daysFromToday = mutableIntStateOf(0)
    val error = mutableStateOf("")
    val data = mutableStateOf<WeatherData?>(null)
    val lastUpdated = mutableStateOf("")

    val latitude = mutableDoubleStateOf(0.0)
    val longitude = mutableDoubleStateOf(0.0)
    var errorMsg by mutableStateOf("")

    fun setErrMsg(resourceText: String){
        errorMsg = resourceText
    }
    fun fetchData() {
        viewModelScope.launch(Dispatchers.IO) {
            val fetchedData = dataFetcher.fetch(url + "${latitude.doubleValue},${longitude.doubleValue}")
            if (fetchedData == "") {
                error.value = errorMsg
            } else {
                data.value = Gson().fromJson(fetchedData, WeatherData::class.java)
                lastUpdated.value = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy"))
            }
        }
    }
}