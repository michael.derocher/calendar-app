package com.example.calendarproject.Presentation.screens.weather

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.calendarproject.Domain.Classes.DailyForecast
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import java.time.LocalDate
import com.example.calendarproject.Presentation.ViewModels.Tab
import com.example.calendarproject.R
import kotlinx.coroutines.delay
import kotlin.math.roundToInt

@Composable
fun WeatherForecastScreen(viewModel : WeatherViewModel, navController: NavController) {
    var minuteCounter by rememberSaveable { mutableIntStateOf(0) }

    LaunchedEffect(minuteCounter) {
        while (true) {
            delay(1000 * 60 * 10)
            viewModel.fetchData()
            minuteCounter++
        }
    }

    if (viewModel.error.value != "") {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(viewModel.error.value)
            BackToDayScreenButton(navController)
        }
        return
    }

    if (viewModel.latitude.value == 0.0 || viewModel.longitude.value == 0.0) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(stringResource(R.string.get_location))
            BackToDayScreenButton(navController)
        }
        return
    }

    if (viewModel.data.value == null) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(stringResource(R.string.fetch_data))
            BackToDayScreenButton(navController)
        }
        return
    }

    val today = LocalDate.now()
    val amountOfDaysOfWeek = viewModel.data.value!!.futureForecast.forecastDay.size
    val daysOfWeek = truncateStrings(getNextDaysOfWeek(today, amountOfDaysOfWeek), 3)

    Column (modifier = Modifier
        .fillMaxSize()
        .verticalScroll(rememberScrollState())
    ){
        if (viewModel.daysFromToday.value == 0) {
            CurrentForecast(viewModel, navController)
        } else {
            Text(text = "${stringResource(R.string.last_updated)}: ${viewModel.lastUpdated.value}", modifier = Modifier
                .padding(15.dp)
                .testTag("last_updated"))
        }
        Tabs(viewModel)
        HourlyForecasts(viewModel)
        DailyForecasts(viewModel, daysOfWeek)
        if (viewModel.daysFromToday.value != 0) {
            BackToDayScreenButton(navController)
        }
    }
}

@Composable
fun BackToDayScreenButton(navController: NavController) {
    Button(
        modifier = Modifier
            .padding(start = 15.dp)
            .testTag("back_button"),
        onClick = { navController.navigate(NavRoutes.Day.route + "/month") }
    ) {
        Text(text =stringResource(R.string.bottom_bar_back), fontSize = 20.sp)
    }
}
@Composable
fun CurrentForecast(viewModel : WeatherViewModel, navController: NavController){
    Column (
        modifier = Modifier.padding(15.dp),
        verticalArrangement = Arrangement.spacedBy(20.dp)
    ){
        Row (
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            Text(text = stringResource(R.string.weather), fontSize = 35.sp, fontWeight = FontWeight.W500, modifier = Modifier.testTag("weather"))
            BackToDayScreenButton(navController)
        }

        Text(text = "${stringResource(R.string.last_updated)}: ${viewModel.lastUpdated.value}", modifier = Modifier.testTag("last_updated"))
        
        Row (
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            Column {
                Text(text = stringResource(R.string.now), fontSize = 25.sp, modifier = Modifier.testTag("now"))
                Row {
                    Text(text = "${viewModel.data.value!!.currentForecast.temperature}°", fontSize = 50.sp, fontWeight = FontWeight.Bold, modifier = Modifier.testTag("current_temp"))
                    WeatherIcon(viewModel.data.value!!.currentForecast.condition.iconUrl, 70.dp)
                }
                Text("${stringResource(R.string.feels_like)} ${viewModel.data.value!!.currentForecast.feelsLike}°", modifier = Modifier.testTag("feels_like"))
            }
            Column {
                Text(text = viewModel.data.value!!.currentForecast.condition.description, fontSize = 20.sp, modifier = Modifier.testTag("current_description"))
                Text(text = "${stringResource(R.string.precipitation)}: ${viewModel.data.value!!.currentForecast.precipitation} mm", fontSize = 15.sp, modifier = Modifier.testTag("current_precipitation"))
                Text(text = "${stringResource(R.string.humidity)}: ${viewModel.data.value!!.currentForecast.humidity} %", fontSize = 15.sp, modifier = Modifier.testTag("current_humidity"))
                Text(text = "${stringResource(R.string.wind)}: ${viewModel.data.value!!.currentForecast.windSpeed} km/h", fontSize = 15.sp, modifier = Modifier.testTag("current_wind"))
            }
        }
    }
}

@Composable
fun Tabs(viewModel : WeatherViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 15.dp, bottom = 20.dp),
        horizontalArrangement = Arrangement.SpaceEvenly
    ){
        Tab(stringResource(R.string.overview),
            Modifier
                .clickable { viewModel.selectedTab.value = Tab.OVERVIEW }
                .testTag("overview_tab"), viewModel)
        Tab(stringResource(R.string.precipitation),
            Modifier
                .clickable { viewModel.selectedTab.value = Tab.PRECIPITATION }
                .testTag("precipitation_tab"), viewModel)
        Tab(stringResource(R.string.wind),
            Modifier
                .clickable { viewModel.selectedTab.value = Tab.WIND }
                .testTag("wind_tab"), viewModel)
        Tab(stringResource(R.string.humidity),
            Modifier
                .clickable { viewModel.selectedTab.value = Tab.HUMIDITY }
                .testTag("humidity_tab"), viewModel)
    }
}

@Composable
fun Tab(text : String, modifier : Modifier, viewModel: WeatherViewModel) {
    Column (
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ){
        Text(text = text, modifier = modifier)
        val color = if (viewModel.selectedTab.value.toString().lowercase() == text.lowercase()) Color.Blue else Color.White
        val width = when (text) {
            stringResource(R.string.overview) -> { 70.dp }
            stringResource(R.string.precipitation) -> { 95.dp }
            stringResource(R.string.wind) -> { 40.dp }
            stringResource(R.string.humidity) -> { 70.dp }
            else -> { 0.dp }
        }
        SelectedIdentifier(color, 5.dp, width)
    }
}

@Composable
fun HourlyForecasts(viewModel : WeatherViewModel) {
    val isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE
    val arrangement = if (isLandscape) Arrangement.SpaceBetween else Arrangement.spacedBy(25.dp)

    Row (
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)
            .horizontalScroll(rememberScrollState()),
        horizontalArrangement = arrangement
    ){
        for (i in 0..7) {
            HourlyForecast(viewModel, i)
        }
    }
}

@Composable
fun HourlyForecast(viewModel : WeatherViewModel, i : Int) {
    Column(
        modifier = Modifier.width(80.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        Text(text = (i * 3).toString() + ":00", fontWeight = FontWeight.Bold)
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            var text = ""
            var imageUrl = ""

            when (viewModel.selectedTab.value) {
                Tab.OVERVIEW -> {
                    imageUrl = viewModel.data.value!!.futureForecast.forecastDay[viewModel.selectedDayIndex.value].hourlyForecasts[i*3].condition.iconUrl
                    text = "${viewModel.data.value!!.futureForecast.forecastDay[viewModel.selectedDayIndex.value].hourlyForecasts[i*3].temperature}°"
                }

                Tab.PRECIPITATION -> {
                    imageUrl = "precipitation"
                    text = "${viewModel.data.value!!.futureForecast.forecastDay[viewModel.selectedDayIndex.value].hourlyForecasts[i*3].precipitation} mm"
                }

                Tab.WIND -> {
                    imageUrl = "wind"
                    text = "${viewModel.data.value!!.futureForecast.forecastDay[viewModel.selectedDayIndex.value].hourlyForecasts[i*3].windSpeed} km/h"
                }

                Tab.HUMIDITY -> {
                    imageUrl = "humidity"
                    text = "${viewModel.data.value!!.futureForecast.forecastDay[viewModel.selectedDayIndex.value].hourlyForecasts[i*3].humidity}%"
                }
            }

            WeatherIcon(imageUrl, 35.dp)
            Text(text)
        }
    }
}

@Composable
fun DailyForecasts(viewModel : WeatherViewModel, daysOfWeek : List<String>) {
    val isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE
    val arrangement = if (isLandscape) Arrangement.SpaceEvenly else Arrangement.spacedBy(10.dp)

    Row (
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)
            .horizontalScroll(rememberScrollState()),
        horizontalArrangement = arrangement
    ){
        for (i in viewModel.daysFromToday.value .. daysOfWeek.size - 1) {
            DailyForecast(viewModel, daysOfWeek[i], viewModel.data.value!!.futureForecast.forecastDay[i].dailyForecast, i)
        }
    }
}

@Composable
fun DailyForecast(viewModel: WeatherViewModel, dayOfWeek : String, dailyForecast: DailyForecast, i : Int) {
    Column (
        modifier = Modifier
            .border(
                width = 1.dp,
                color = Color.Black,
                shape = RoundedCornerShape(10.dp)
            )
            .padding(5.dp)
            .width(75.dp)
            .height(90.dp)
            .clickable {
                viewModel.selectedDayIndex.value = i
            },
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Text(text = dayOfWeek, fontWeight = FontWeight.Bold)
        WeatherIcon(viewModel.data.value!!.futureForecast.forecastDay[i].dailyForecast.condition.iconUrl, 35.dp)
        Text(text = "${dailyForecast.minTemperature.roundToInt()}°/${dailyForecast.maxTemperature.roundToInt()}°")
        val color = if (viewModel.selectedDayIndex.value == i) Color.Blue else Color.White
        SelectedIdentifier(color, 7.dp, 70.dp)
    }
}

@Composable
fun WeatherIcon(url : String, size : Dp) {
    val id = getResourceId(LocalContext.current, url)
    if (id == 0) {
        AsyncImage(model = "https:${url}", contentDescription = stringResource(R.string.icon), modifier = Modifier.size(size))
    } else {
        Image(
            painter = painterResource(id),
            contentDescription = stringResource(R.string.icon),
            modifier = Modifier.size(35.dp)
        )
    }
}

fun getResourceId(context : Context, name : String): Int {
    return try {
        val resources: Resources = context.resources
        val packageName: String = context.packageName

        resources.getIdentifier(name, "drawable", packageName)
    } catch (e: Exception) {
        e.printStackTrace()
        0
    }
}

@Composable
fun SelectedIdentifier(color : Color, height : Dp, width : Dp) {
    Row (
        modifier = Modifier
            .background(color)
            .width(width)
            .height(height)
    ){}
}

fun getNextDaysOfWeek(startingDate : LocalDate, nextDaysOfWeekAmount : Int) : List<String> {
    return (0..nextDaysOfWeekAmount - 1).map {i ->
        val nextDay = startingDate.plusDays(i.toLong())
        nextDay.dayOfWeek.toString()
    }
}

fun truncateStrings(strings: List<String>, truncAmount: Int) : List<String> {
    return strings.map { string -> string.substring(0, truncAmount) }
}

