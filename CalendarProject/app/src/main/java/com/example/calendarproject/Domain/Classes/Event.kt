package com.example.calendarproject.Domain.Classes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events")
class Event {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0

    @ColumnInfo(name = "title")
    var title: String = ""

    @ColumnInfo(name = "subject")
    var subject: String = ""

    @ColumnInfo(name = "date")
    var date: String = ""

    @ColumnInfo(name = "startTime")
    var startTime: String = ""

    @ColumnInfo(name = "endTime")
    var endTime: String = ""

    @ColumnInfo(name = "location")
    var location: String = ""

    @ColumnInfo(name = "description")
    var description: String = ""

    constructor()

    constructor(title: String, subject: String, date: String, startTime: String, endTime: String, location: String, description: String) {
        this.title = title
        this.subject = subject
        this.date = date
        this.startTime = startTime
        this.endTime = endTime
        this.location = location
        this.description = description
    }
}