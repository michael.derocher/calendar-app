package com.example.calendarproject.Presentation.screens.Event

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.widget.DatePicker
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.calendarproject.Domain.Classes.EventError
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.R
import java.util.Calendar

@Composable
fun DateSelection(context: Context = LocalContext.current,
                  monthViewModel: MonthViewModel,
                  onDateChange: (String) -> Unit) : DatePickerDialog{
    val datePieces = monthViewModel.getCurrentDate().split("/")
    val year = datePieces[2].toInt()
    val month = datePieces[0].toInt() - 1
    val dayOfMonth = datePieces[1].toInt()

    val datePicker = DatePickerDialog(
        context,
        { _: DatePicker, selectedYear: Int, selectedMonth: Int, selectedDayOfMonth: Int ->
            onDateChange("${selectedDayOfMonth.toString().padStart(2, '0')}/${(selectedMonth + 1).toString().padStart(2, '0')}/$selectedYear")
        }, year, month, dayOfMonth
    )

    Column(modifier = Modifier
        .testTag("dateColumn")){
        Text("")
    }

    return datePicker
}

@Composable
fun TimeSelection(context: Context = LocalContext.current,
                  calendar: Calendar,
                  onTimeChange: (String) -> Unit) : TimePickerDialog{
    val hour = calendar[Calendar.HOUR_OF_DAY]
    val minute = calendar[Calendar.MINUTE]

    val timePicker = TimePickerDialog(
        context,
        { _, selectedHour: Int, selectedMinute: Int ->
            onTimeChange("$selectedHour:${processMinute(selectedMinute)}")
        }, hour, minute, true
    )

    Column(modifier = Modifier
        .testTag("timeColumn")){
        Text("")
    }

    return timePicker
}

fun processMinute(minute: Int) : String{
    if (minute < 10){
        return minute.toString().padStart(2, '0')
    }
    return minute.toString()
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GeneralField(heading: String, inputText: String, placeHolderText: String, onInputTextChange: (String) -> Unit){
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 10.dp, bottom = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween) {
        Text(heading,
            fontWeight = FontWeight.Bold,
            fontSize = 25.sp,
            modifier = Modifier.padding(end = 10.dp))
        TextField(value = inputText, onValueChange = { onInputTextChange(it) },
            modifier = Modifier
                .width(250.dp)
                .height(50.dp)
                .padding(end = 30.dp),
            placeholder = {Text(placeHolderText)}
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DescriptionField(descriptionText: String, placeHolderText: String, onDescriptionTextChange: (String) -> Unit){
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 10.dp, bottom = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween) {
        Text("${stringResource(R.string.viewfield_description)}:",
            fontWeight = FontWeight.Bold,
            fontSize = 25.sp,
            modifier = Modifier.padding(end = 10.dp))
    }
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(bottom = 30.dp)
        .width(IntrinsicSize.Max)
    ) {
        TextField(value = descriptionText, onValueChange = { onDescriptionTextChange(it) },
            modifier = Modifier
                .height(120.dp)
                .padding(start = 10.dp, end = 30.dp)
                .verticalScroll(rememberScrollState())
                .weight(1f),
            placeholder = {Text(placeHolderText)}
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DateTimeField(type: String, picker: AlertDialog, value: String, placeHolderText: String) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 10.dp, bottom = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween) {
        Text(type,
            fontWeight = FontWeight.Bold,
            fontSize = 25.sp,
            modifier = Modifier.padding(top = 5.dp, end = 10.dp))
        TextField(
            value = value,
            onValueChange = { },
            enabled = false,
            modifier = Modifier
                .width(250.dp)
                .height(50.dp)
                .padding(end = 30.dp)
                .clickable { picker.show() },
            placeholder = {Text(placeHolderText)}
        )
    }
}

@Composable
fun CreateDecision(navController: NavController, viewModel: EventViewModel, creatingFromMonthScreen: Boolean, onErrorChange: (EventError) -> Unit){
    val errorTitle = stringResource(R.string.error_occurred)
    val errorFormDesc = stringResource(R.string.form_empty)
    val errorOverlapDesc = "${stringResource(R.string.this_event)} ${viewModel.date} ${stringResource(R.string.error_from)} ${viewModel.startTime} ${stringResource(R.string.error_to)} ${viewModel.endTime} ${stringResource(R.string.error_overlap)}"
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 10.dp)) {
        Button(onClick = {
            if (viewModel.containsEmptyFields()) {
                onErrorChange(EventError(true, errorTitle, errorFormDesc))
            } else if (viewModel.hasOverlappingEvents()) {
                onErrorChange(EventError(true, errorTitle, errorOverlapDesc))
            } else {
                viewModel.createEvent()
                //navController.navigate(NavRoutes.Day.route + "/add")
                navController.navigate(NavRoutes.View.route)
            }
        }){
            Text(stringResource(R.string.create), fontSize = 20.sp)
        }
        Spacer(modifier = Modifier.width(15.dp))
        Button(onClick = {
            viewModel.resetNewEvent()
            if (creatingFromMonthScreen) {
                navController.navigate(NavRoutes.Month.route)
            } else {
                navController.navigate(NavRoutes.Day.route + "/add")
            }
        }
        ){
            Text(stringResource(R.string.cancel), fontSize = 20.sp)
        }
    }
}

@Composable
fun EditDecision(navController: NavController, viewModel: EventViewModel, onErrorChange: (EventError) -> Unit){
    val errorTitle = stringResource(R.string.error_occurred)
    val errorFormDesc = stringResource(R.string.form_empty)
    val errorOverlapDesc = "${stringResource(R.string.this_event)} ${viewModel.date} ${stringResource(R.string.error_from)} ${viewModel.startTime} ${stringResource(R.string.error_to)} ${viewModel.endTime} ${stringResource(R.string.error_overlap)}"
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(start = 10.dp)) {
        Button(onClick = {
            if (viewModel.containsEmptyFields()) {
                onErrorChange(EventError(true, errorTitle, errorFormDesc))
            } else if (viewModel.hasOverlappingEvents()) {
                onErrorChange(EventError(true, errorTitle, errorOverlapDesc))
            } else {
                viewModel.confirmEdit()
                navController.navigate(NavRoutes.View.route)
            }
        }){
            Text(stringResource(R.string.viewfield_edit), fontSize = 20.sp)
        }
        Spacer(modifier = Modifier.width(15.dp))
        Button(onClick = {
            viewModel.toggleEventSet()
            navController.navigate(NavRoutes.View.route)
        }){
            Text(stringResource(R.string.cancel), fontSize = 20.sp)
        }
    }
}