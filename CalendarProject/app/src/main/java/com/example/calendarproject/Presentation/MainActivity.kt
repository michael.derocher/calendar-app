package com.example.calendarproject.Presentation

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Geocoder.GeocodeListener
import android.location.LocationRequest
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.calendarproject.Domain.Classes.EventError
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.screens.Day.DailyOverviewScreen
import com.example.calendarproject.Presentation.screens.Event.CreateDecision
import com.example.calendarproject.Presentation.screens.Event.EditDecision
import com.example.calendarproject.Presentation.screens.Event.ManipulateEvent
import com.example.calendarproject.Presentation.screens.Event.Popup
import com.example.calendarproject.Presentation.screens.Event.ViewEvent
import com.example.calendarproject.Presentation.screens.Month.Month
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import com.example.calendarproject.Presentation.managers.LocationManager
import com.example.calendarproject.R
import com.example.calendarproject.Presentation.screens.weather.WeatherForecastScreen
import com.google.android.gms.location.LocationServices
import java.time.LocalDate
import java.util.Calendar
import java.util.Locale

class MainActivity : ComponentActivity() {
    private var weatherViewModel : WeatherViewModel ?= null

    private val locationPermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                LocationManager.Builder
                    .create(this@MainActivity)
                    .request(onUpdateLocation = { latitude, longitude ->
                        LocationManager.removeCallback(this@MainActivity)

                        weatherViewModel?.latitude?.value = latitude
                        weatherViewModel?.longitude?.value = longitude
                        weatherViewModel?.fetchData()
                    })
            }

            permissions.getOrDefault(Manifest.permission.ACCESS_COARSE_LOCATION, false) -> {
                LocationManager.Builder
                    .create(this@MainActivity)
                    .request(onUpdateLocation = { latitude, longitude ->
                        LocationManager.removeCallback(this@MainActivity)

                        weatherViewModel?.latitude?.value = latitude
                        weatherViewModel?.longitude?.value = longitude
                        weatherViewModel?.fetchData()
                    })
            }

            else -> {
                LocationManager.goSettingScreen(this)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        weatherViewModel = ViewModelProvider(this, WeatherViewModelFactory())[WeatherViewModel::class.java]

        locationPermissionRequest.launch(arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ))

        setContent {
            CalendarProjectTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CalendarMenu(weatherViewModel!!)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CalendarMenu(weatherViewModel : WeatherViewModel) {
    val navController = rememberNavController()

    val monthViewModel : MonthViewModel
    val eventViewModel : EventViewModel
    val dayViewModel : DayViewModel
    val context: Context = LocalContext.current

    weatherViewModel.setErrMsg(stringResource(R.string.fetch_error))

    val owner = LocalViewModelStoreOwner.current

    owner?.let {
        eventViewModel = viewModel(
            it,
            "EventViewModel",
            EventViewModelFactory(
                LocalContext.current.applicationContext
                        as Application
            )
        )

        monthViewModel = viewModel(
            it,
            "MonthViewModel",
            MonthViewModelFactory(
                LocalContext.current.applicationContext
                        as Application, context
            )
        )

        dayViewModel = viewModel(
            it,
            "DayViewModel",
            DayViewModelFactory(
                LocalContext.current.applicationContext
                        as Application, context
            )
        )

        Scaffold(
            content = { padding ->
                Column(
                    Modifier
                        .padding(padding)
                        .fillMaxSize()) {

                    NavigationHost(navController = navController, monthViewModel, eventViewModel, dayViewModel, weatherViewModel)
                }
            }
        )
    }
}

@Composable
fun NavigationHost(navController: NavHostController, monthViewModel : MonthViewModel, eventViewModel : EventViewModel, dayViewModel: DayViewModel, weatherViewModel: WeatherViewModel){
    val context: Context = LocalContext.current
    getLocationCountryCode(context, dayViewModel)
    NavHost(navController = navController, startDestination = NavRoutes.Month.route)
    {
        composable(NavRoutes.Month.route) {
            monthViewModel.updateDaysContainingEvents()
            Month(navController, monthViewModel, dayViewModel, eventViewModel)
        }

        composable(NavRoutes.Day.route + "/{fromView}") { backStackEntry ->
            dayViewModel.updateDateEvents()
            dayViewModel.fetchYearHolidays(dayViewModel.currentYear.toString(), dayViewModel.country_code)
            dayViewModel.updateDailyHolidays()
            DailyOverviewScreen(navController = navController, weatherViewModel = weatherViewModel, dayViewModel = dayViewModel, eventViewModel = eventViewModel)
        }

        composable(NavRoutes.View.route) {
            ViewEvent(navController, eventViewModel, dayViewModel)
        }

        composable(NavRoutes.Add.route + "/{fromView}") { backStackEntry ->
            val fromView = backStackEntry.arguments?.getString("fromView")

            var error by rememberSaveable {
                mutableStateOf(EventError(false, "", ""))
            }

            eventViewModel.updateEventsInCurrentDay()

            ManipulateEvent(navController = navController,
                            context = LocalContext.current,
                            calendar = Calendar.getInstance(),
                            decision = { navController, eventViewModel -> CreateDecision(navController, eventViewModel, fromView == "month") { error = it } },
                            viewModel = eventViewModel,
                            monthViewModel,
                            displayDate = fromView == "month",
                            specifier = stringResource(R.string.create)
            )

            if (error.display) {
                Popup(error) { error = it }
            }
        }

        composable(NavRoutes.Edit.route) {
            if (!eventViewModel.isEventSet){
                eventViewModel.setEvent(eventViewModel.currentEvent)
                eventViewModel.toggleEventSet()
            }

            var error by rememberSaveable {
                mutableStateOf(EventError(false, "", ""))
            }

            eventViewModel.updateEventsInCurrentDay()

            ManipulateEvent(navController,
                            context = LocalContext.current,
                            calendar = Calendar.getInstance(),
                            decision = { navController, eventViewModel -> EditDecision(navController, eventViewModel) { error = it } },
                            viewModel = eventViewModel,
                            monthViewModel,
                            displayDate = true,
                            specifier = stringResource(R.string.viewfield_edit))

            if (error.display) {
                Popup(error) { error = it }
            }
        }

        composable(NavRoutes.Weather.route) {
            WeatherForecastScreen(weatherViewModel, navController)
        }
    }
}

private fun getLocationCountryCode(context: Context, dayViewModel: DayViewModel){
    if(dayViewModel.country_code == "") {
        dayViewModel.country_code = context.resources.configuration.locales[0].country
        try {
            if (context.let {
                    ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                } == PackageManager.PERMISSION_GRANTED) {
                try {
                    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
                    val location = fusedLocationClient.getCurrentLocation(LocationRequest.QUALITY_HIGH_ACCURACY, null)
                    location.addOnCompleteListener{
                        if (location.result != null) {
                            Geocoder(context, Locale.getDefault()).getFromLocation(
                                location.result.latitude,
                                location.result.longitude,
                                1,
                                GeocodeListener {
                                    Log.e("CountryCode", it.toString());
                                    dayViewModel.country_code = it[0].countryCode
                                    dayViewModel.fetchYearHolidays(
                                        dayViewModel.currentYear.toString(),
                                        dayViewModel.country_code
                                    )
                                })
                        }
                    }
                } catch (e: Exception) {
                    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
                    fusedLocationClient.getCurrentLocation(LocationRequest.QUALITY_BALANCED_POWER_ACCURACY, null)
                    val location = fusedLocationClient.lastLocation
                    location.addOnCompleteListener {
                        if (location.result != null) {
                            Geocoder(context, Locale.getDefault()).getFromLocation(
                                location.result.latitude,
                                location.result.longitude,
                                1,
                                GeocodeListener {
                                    Log.e("CountryCode", it.toString());
                                    dayViewModel.country_code = it[0].countryCode
                                    dayViewModel.fetchYearHolidays(
                                        dayViewModel.currentYear.toString(),
                                        dayViewModel.country_code
                                    )
                                })
                        }
                    }
                    Log.e("CountryCode", e.message!!)
                }
            }
        } catch (e : java.lang.NullPointerException){
            Log.e("CountryCode", e.message!!)
        }
    }
}

class DayViewModelFactory(val application: Application, context: Context) :
    ViewModelProvider.Factory {
    var dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DayViewModel(LocalDate.now(), application, dayVMStrings) as T
    }
}

class MonthViewModelFactory(val application: Application, context: Context) :
    ViewModelProvider.Factory {
    var monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MonthViewModel(application, monthVMStrings) as T
    }
}


class EventViewModelFactory(val application: Application) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return EventViewModel(application) as T
    }
}

class WeatherViewModelFactory :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return WeatherViewModel() as T
    }
}

@Preview(showBackground = true)
@Composable
fun CalendarMenuPreview() {
    CalendarProjectTheme {
        //CalendarMenu()
    }
}