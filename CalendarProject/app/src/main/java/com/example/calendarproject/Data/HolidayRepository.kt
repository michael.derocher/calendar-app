package com.example.calendarproject.Data

import com.example.calendarproject.Domain.Classes.HolidayResponse
import com.example.calendarproject.Domain.RetrofitInstance

class HolidayRepository {
    private val holidayService = RetrofitInstance.holidayService

    suspend fun getHolidays(year : String, country_code : String): List<HolidayResponse> {
        return holidayService.getHolidays(year, country_code)
//        year, country_code
    }
}