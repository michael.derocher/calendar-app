package com.example.calendarproject.Presentation.screens.Event

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import java.time.LocalDate
import com.example.calendarproject.R
import java.util.Locale

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ViewEvent(navController: NavController, viewModel: EventViewModel, dayViewModel : DayViewModel) {
    var confirmVisibility by rememberSaveable { mutableStateOf(false) }
    val enableConfirmVis = {confirmVisibility = true}
    val disableConfirmVis = {confirmVisibility = false}

    Scaffold(
        content = {padding ->
            Column(Modifier.padding(padding)) {
                Column(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .fillMaxHeight()
                        .height(IntrinsicSize.Max)
                ) {
                    ViewFields(viewModel)
                    OptionLine(navController, confirmVisibility, enableConfirmVis, disableConfirmVis, viewModel)
                }
            } },
        bottomBar = { Button(onClick = {
            val splitDate = viewModel.currentEvent.date.split("/")
            dayViewModel.setDate(LocalDate.of(splitDate[2].toInt(), splitDate[0].toInt(), splitDate[1].toInt()))
            navController.navigate(NavRoutes.Day.route + "/fromViewEvent")
        },
                        modifier = Modifier.padding(start = 10.dp, bottom = 30.dp)
        ){
            Text(stringResource(R.string.bottom_bar_back), fontSize = 20.sp)
        } }
    )
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ViewFields(viewModel: EventViewModel) {
    var dateFormatted = viewModel.currentEvent.date
    if (Locale.getDefault().language.equals("fr")){
        val dateParts = dateFormatted.split("/")
        dateFormatted = "${dateParts[1]}/${dateParts[0]}/${dateParts[2]}"
    }

    Title("${stringResource(R.string.viewfield_title)}: ${viewModel.currentEvent.title}")
    Line("${stringResource(R.string.viewfield_subject)}:", viewModel.currentEvent.subject)
    Line("${stringResource(R.string.viewfield_date)}:", dateFormatted)
    Line("${stringResource(R.string.viewfield_start)}:", viewModel.currentEvent.startTime)
    Line("${stringResource(R.string.viewfield_end)}:", viewModel.currentEvent.endTime)
    Line("${stringResource(R.string.viewfield_location)}:", viewModel.currentEvent.location)
    Description(viewModel.currentEvent.description)
}