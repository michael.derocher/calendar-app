package com.example.calendarproject.Presentation.screens.Event

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.calendarproject.Domain.Classes.EventError
import com.example.calendarproject.R

@Composable
fun Popup(error: EventError, onErrorChange: (EventError) -> Unit) {
    Column (
        modifier = Modifier.fillMaxSize().clip(RoundedCornerShape(10)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ){
        Row (
            modifier = Modifier
                .width(350.dp)
                .height(250.dp)
                .clip(RoundedCornerShape(10)),
            verticalAlignment = Alignment.Bottom
        ){
            Column (
                modifier = Modifier.fillMaxSize().background(Color.DarkGray).padding(15.dp).clip(
                    RoundedCornerShape(10)),
                verticalArrangement = Arrangement.SpaceEvenly,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = error.title, color = Color.White, fontSize = 30.sp)
                Text(text = error.description, color = Color.White, fontSize = 22.sp)
                Button(
                    onClick = { onErrorChange(EventError(false, "", "")) }
                ) {
                    Text(text = stringResource(R.string.popup_close), fontSize = 20.sp)
                }
            }
        }
    }
}











