package com.example.calendarproject.Domain.Classes

data class HolidayResponse(
    val date : String,
    val localName : String
)
