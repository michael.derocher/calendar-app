package com.example.calendarproject.Presentation.screens.Event

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.R

@Composable
fun Title(title: String){
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 30.dp)
    ) {
        Text(title,
            fontSize = 35.sp,
            modifier = Modifier.padding(top = 10.dp),
            fontWeight = FontWeight.Bold,)
    }
}

@Composable
fun Line(heading: String, details: String){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween) {
        Text(heading,
            fontWeight = FontWeight.Bold,
            fontSize = 25.sp,
            modifier = Modifier.padding(end = 10.dp))
        Text(details,
            modifier = Modifier.padding(end = 100.dp),
            fontSize = 25.sp
        )
    }
}

@Composable
fun Description(details: String){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 20.dp),
        horizontalArrangement = Arrangement.SpaceBetween) {
        Text("${stringResource(R.string.viewfield_description)}:",
            fontWeight = FontWeight.Bold,
            fontSize = 25.sp,
            modifier = Modifier.padding(end = 10.dp))
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 30.dp)
            .width(IntrinsicSize.Max)
    ) {
        Text(details,
            modifier = Modifier
                .padding(start = 10.dp, end = 30.dp)
                .verticalScroll(rememberScrollState())
                .weight(1f),
            fontSize = 25.sp
        )
    }
}

@Composable
fun OptionLine(navController: NavController,
               confirmVisibility: Boolean,
               enableConfirmVis: () -> Unit, disableConfirmVis: () -> Unit,
               eventViewModel: EventViewModel
){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 10.dp),
        horizontalArrangement = Arrangement.Center) {
        Button(onClick = {navController.navigate(NavRoutes.Edit.route)}
        ){
            Text(stringResource(R.string.viewfield_edit), fontSize = 20.sp)
        }
        Spacer(modifier = Modifier.width(20.dp))
        Button(onClick = {enableConfirmVis()}
        ){
            Text(stringResource(R.string.viewfield_delete), fontSize = 20.sp)
        }
    }
    if (confirmVisibility){
        ConfirmDelete(navController, disableConfirmVis, eventViewModel)
    }
}

@Composable
fun ConfirmDelete(navController: NavController, disableConfirmVis: () -> Unit, eventViewModel: EventViewModel){
    Column(modifier = Modifier
        .fillMaxWidth()
        .width(IntrinsicSize.Max),
        horizontalAlignment = Alignment.CenterHorizontally){
        Text(stringResource(R.string.viewfield_confirm))
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 10.dp),
            horizontalArrangement = Arrangement.Center) {
            Button(onClick = {
                eventViewModel.deleteEvent(eventViewModel.currentEvent.id)
                navController.navigate(NavRoutes.Day.route + "/fromDeleteEvent")
            }
            ){
                Text(stringResource(R.string.viewfield_yes), fontSize = 20.sp)
            }
            Spacer(modifier = Modifier.width(10.dp))
            Button(onClick = {disableConfirmVis()}
            ){
                Text(stringResource(R.string.viewfield_no), fontSize = 20.sp)
            }
        }
    }
}