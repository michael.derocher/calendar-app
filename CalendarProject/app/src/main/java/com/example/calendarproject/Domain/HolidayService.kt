package com.example.calendarproject.Domain

import com.example.calendarproject.Domain.Classes.HolidayResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface HolidayService {
    @GET("{year}/{country_code}")
    suspend fun getHolidays(@Path("year") year : String, @Path("country_code") country_code : String): List<HolidayResponse>

}