package com.example.calendarproject.Presentation.ViewModels

import android.app.Application
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.lifecycle.MutableLiveData
import com.example.calendarproject.Data.EventRepository
import com.example.calendarproject.Data.EventRoomDatabase
import java.util.Calendar
import java.text.DateFormatSymbols
import java.time.LocalDate


class MonthViewModel(application: Application, strings: List<String>) : ViewModel() {
    private val repository : EventRepository

    val calendar: Calendar = Calendar.getInstance()
    var currentMonthScreenDate by mutableStateOf(getCurrentMonthAndYear())
    var daysInCurrentMonth by mutableStateOf(getDaysInMonth())
    val daysOfWeek = strings

    var selectedDate: LocalDate by mutableStateOf(LocalDate.now())
    val daysContainingEvents: MutableLiveData<List<Int>>

    init {
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val eventDb = EventRoomDatabase.getInstance(application)
        val eventDao = eventDb.eventDao()
        repository = EventRepository(eventDao)

        updateDaysContainingEvents()
        daysContainingEvents = repository.daysContainingEvents
    }

    fun updateDaysContainingEvents() {
        val monthIndex = calendar.get(Calendar.MONTH) + 1
        val currentYear = calendar.get(Calendar.YEAR)
        repository.updateDaysContainingEvents(monthIndex.toString().padStart(2, '0'), currentYear.toString())
    }

    fun nextMonth() {
        calendar.add(Calendar.MONTH, 1)
        currentMonthScreenDate = getCurrentMonthAndYear()
        daysInCurrentMonth = getDaysInMonth()
        updateDaysContainingEvents()
    }

    fun previousMonth() {
        calendar.add(Calendar.MONTH, -1)
        currentMonthScreenDate = getCurrentMonthAndYear()
        daysInCurrentMonth = getDaysInMonth()
        updateDaysContainingEvents()
    }

    fun getStartingDayOfWeek() : Int {
        return calendar[Calendar.DAY_OF_WEEK] - 1
    }

    fun getCurrentDate(): String{
        val currentDate = Calendar.getInstance()
        var monthFormatted = (calendar.get(Calendar.MONTH) + 1).toString().padStart(2, '0')
        var dayFormatted = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(2, '0')
        var yearFormatted = calendar.get(Calendar.YEAR).toString()

        if (calendar.get(Calendar.DAY_OF_MONTH) <= currentDate.get(Calendar.DAY_OF_MONTH)){
            if (calendar.get(Calendar.YEAR) <= currentDate.get(Calendar.YEAR)){
                monthFormatted = (currentDate.get(Calendar.MONTH) + 1).toString().padStart(2, '0')
                dayFormatted = currentDate.get(Calendar.DAY_OF_MONTH).toString().padStart(2, '0')
                yearFormatted = currentDate.get(Calendar.YEAR).toString()
            }
        }

        return "$monthFormatted/$dayFormatted/$yearFormatted"
    }

    private fun getCurrentMonthAndYear() : String {
        return "${getCurrentMonthString()} ${getCurrentYear()}"
    }

    private fun getDaysInMonth() : Int {
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    }

    private fun getCurrentMonthString(): String {
        val month = calendar.get(Calendar.MONTH)
        val dateFormatSymbols = DateFormatSymbols()
        val months = dateFormatSymbols.months
        return months[month]
    }

    private fun getCurrentYear() : Int {
        return calendar.get(Calendar.YEAR)
    }
}