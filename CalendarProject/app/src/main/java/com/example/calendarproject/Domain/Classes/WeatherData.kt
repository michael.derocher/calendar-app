package com.example.calendarproject.Domain.Classes

import com.google.gson.annotations.SerializedName

data class WeatherData (
    @SerializedName("current")
    val currentForecast : Current,
    @SerializedName("forecast")
    val futureForecast : ForecastDay
)
data class ForecastDay (
    @SerializedName("forecastday")
    val forecastDay : ArrayList<FutureForecast>
)

data class FutureForecast (
    @SerializedName("day")
    val dailyForecast : DailyForecast,
    @SerializedName("hour")
    val hourlyForecasts : ArrayList<HourlyForecast>
)

data class HourlyForecast (
    @SerializedName("temp_c")
    val temperature : Float,
    @SerializedName("condition")
    val condition : Condition,
    @SerializedName("wind_kph")
    val windSpeed : Float,
    @SerializedName("humidity")
    val humidity : Float,
    @SerializedName("precip_mm")
    val precipitation : Float,
)

data class DailyForecast (
    @SerializedName("maxtemp_c")
    val maxTemperature : Float,
    @SerializedName("mintemp_c")
    val minTemperature : Float,
    @SerializedName("condition")
    val condition : Condition,
)

data class Current (
    @SerializedName("last_updated")
    val date : String,
    @SerializedName("temp_c")
    val temperature : Float,
    @SerializedName("condition")
    val condition : Condition,
    @SerializedName("wind_kph")
    val windSpeed : Float,
    @SerializedName("humidity")
    val humidity : Float,
    @SerializedName("precip_mm")
    val precipitation : Float,
    @SerializedName("feelslike_c")
    val feelsLike : Float,
)

data class Condition (
    @SerializedName("text")
    val description : String,
    @SerializedName("icon")
    val iconUrl : String,
)