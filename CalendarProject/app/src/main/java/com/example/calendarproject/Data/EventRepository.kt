package com.example.calendarproject.Data

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import com.example.calendarproject.Domain.Classes.DayEvent
import com.example.calendarproject.Domain.Classes.Event

class EventRepository (private val eventDao: EventDao) {
    val daysContainingEvents = MutableLiveData<List<Int>>()
    val dailyEvents = MutableLiveData<List<DayEvent>>()
    val eventsInCurrentDay = MutableLiveData<List<Event>>()

    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    fun insertEvent(newEvent: Event) {
        runBlocking {
            eventDao.insertEvent(newEvent)
        }
    }

    fun getNewEventId(): Int{
        var newId = 0
        runBlocking { newId = eventDao.getNewEventId() }
        return newId
    }

    fun deleteEvent(id: Int) {
        coroutineScope.launch(Dispatchers.IO) {
            eventDao.deleteEvent(id)
        }
    }

    fun updateEvent(event: Event) {
        coroutineScope.launch(Dispatchers.IO) {
            eventDao.updateEvent(event.title, event.subject, event.date, event.startTime, event.endTime,
                event.location, event.description, event.id)
        }
    }

    fun updateDaysContainingEvents(month: String, year: String) {
        coroutineScope.launch(Dispatchers.IO) {
            try {
                val events = eventDao.getMonthEvents(month, year)
                val results = mutableListOf<Int>()

                events?.let {
                    results.addAll(it.flatMap { event ->
                        val dateParts = event.date.split("/")
                        dateParts.getOrNull(1)?.toIntOrNull()
                            ?.let { day -> listOf(day) } ?: emptyList()
                    })
                }

                daysContainingEvents.postValue(results)
            } catch (e: Exception) {

            }
        }
    }

    fun getDateEvents(date: String) {
        coroutineScope.launch(Dispatchers.IO) {
            asyncFindDateEvents(date)
            val events = eventDao.getDateEvents(date)
            dailyEvents.postValue(convertEventsToDayEvents(events))
        }
    }


    private fun asyncFindDateEvents(date: String)
    {
        coroutineScope.launch(Dispatchers.IO) {
            val events = eventDao.getDateEvents(date)
            eventsInCurrentDay.postValue(events)
        }
    }


    fun getEvent(id: Int) {
        coroutineScope.launch(Dispatchers.IO) {
            eventDao.getEvent(id)
        }
    }

    private fun convertEventsToDayEvents(events : List<Event>) : List<DayEvent>{
        val dayEvents = ArrayList<DayEvent>()
        for(event in events){
            dayEvents.add(
                DayEvent(
                    event.title,
                    event.startTime,
                    event.endTime,
                    event
                )
            )
        }
        return sortByStartTime(dayEvents.toList())
    }

    private fun sortByStartTime(eventList : List<DayEvent>) : List<DayEvent>{
        val sortedList = ArrayList<DayEvent>()
        for (event in eventList.sortedBy { it.startTime }){
            sortedList.add(event)
        }
        return sortedList.toList()
    }
}