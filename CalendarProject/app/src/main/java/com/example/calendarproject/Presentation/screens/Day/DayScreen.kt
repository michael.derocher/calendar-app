package com.example.calendarproject.Presentation.screens.Day

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Shapes
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTagsAsResourceId
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.R
import com.example.calendarproject.Domain.Classes.DayEvent
import com.example.calendarproject.Domain.Classes.Event
import java.time.temporal.ChronoUnit
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import java.time.LocalDate
import java.time.Month
import java.util.Calendar
import java.util.Locale

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DailyOverviewScreen(navController: NavController, weatherViewModel: WeatherViewModel, dayViewModel: DayViewModel, eventViewModel: EventViewModel){
    var selectedDatePieces by rememberSaveable{mutableStateOf(dayViewModel.getCalendarDateString())}
    val onSelectedDateChange = {newDate: String -> selectedDatePieces = newDate}

    var selectedDate: String
    if (Locale.getDefault().language.equals("en")){
        selectedDate = "${selectedDatePieces.split("/")[2]}/${selectedDatePieces.split("/")[0]}/${selectedDatePieces.split("/")[1]}"
    }
    else{
        selectedDate = "${selectedDatePieces.split("/")[2]}/${selectedDatePieces.split("/")[1]}/${selectedDatePieces.split("/")[0]}"
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .semantics { testTagsAsResourceId = true }
            .testTag("MainColumn")
    ) {
        DateNavigator(dayViewModel, onSelectedDateChange)
        NavigationBar(navController, dayViewModel, eventViewModel, selectedDate)
        DisplayHolidays(dayViewModel)
        WeatherButton(weatherViewModel, dayViewModel, navController)
        DailySchedule(navController, dayViewModel, eventViewModel)
    }
}

@Composable
fun WeatherButton(weatherViewModel: WeatherViewModel, dayViewModel: DayViewModel, navController: NavController) {
    val currentMonth = Month.of(dayViewModel.getCurrentMonthIndex() + 1)
    val currentDate = LocalDate.of(dayViewModel.currentYear, currentMonth, dayViewModel.currentDay)
    val today = LocalDate.now()

    var amountDaysFromNow = 5
    if (weatherViewModel.data.value != null) {
        amountDaysFromNow = weatherViewModel.data.value!!.futureForecast.forecastDay.size
    }

    val amountDaysFromNowDate = today.plusDays(amountDaysFromNow.toLong())
    val isBetweenTodayAndNext5Days = currentDate.isEqual(today) || (currentDate.isAfter(today) && currentDate.isBefore(amountDaysFromNowDate))

    if (isBetweenTodayAndNext5Days) {
        val daysFromToday = ChronoUnit.DAYS.between(today, currentDate)
        weatherViewModel.selectedDayIndex.value = daysFromToday.toInt()
        weatherViewModel.daysFromToday.value = daysFromToday.toInt()

        Row (
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ){
            Button(onClick = { navController.navigate(NavRoutes.Weather.route) }) {
               Text(stringResource(R.string.forecast))
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DateNavigator(dayViewModel: DayViewModel, onSelectedDateChange: (String) -> Unit){
    var topDate = "${dayViewModel.currentWeekday}, ${dayViewModel.currentMonth} ${dayViewModel.displayDayText(dayViewModel.currentDay)}, ${dayViewModel.currentYear}"
    if (Locale.getDefault().language.equals("fr")){
        topDate = "${dayViewModel.currentWeekday}, le ${dayViewModel.displayDayText(dayViewModel.currentDay)} ${dayViewModel.currentMonth}, ${dayViewModel.currentYear}"
    }
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 10.dp, end = 10.dp)
            .semantics { testTagsAsResourceId = true }
            .testTag("NavRow")
    ) {
        IconButton(onClick = {
            dayViewModel.previousDay()
            onSelectedDateChange(dayViewModel.getCalendarDateString())
        }) {
            Icon(
                painter = painterResource(id = R.drawable.baseline_arrow_back_24),
                contentDescription = stringResource(R.string.previous_day),
                modifier = Modifier
                    .semantics { testTagsAsResourceId = true }
                    .testTag("PreviousDay")
            )
        }
        Text(
            text = topDate,
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .semantics { testTagsAsResourceId = true }
                .testTag("DateTextDisplay")
        )
        IconButton(onClick = {
            dayViewModel.nextDay()
            onSelectedDateChange(dayViewModel.getCalendarDateString())
        }) {
            Icon(
                painter = painterResource(id = R.drawable.baseline_arrow_forward_24),
                contentDescription = stringResource(R.string.next_day),
                modifier = Modifier
                    .semantics { testTagsAsResourceId = true }
                    .testTag("NextDay")
            )
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun NavigationBar(navController: NavController, dayViewModel: DayViewModel, eventViewModel: EventViewModel, selectedDate: String){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 10.dp, end = 10.dp)
            .semantics { testTagsAsResourceId = true }
            .testTag("NavbarRow"),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Row(modifier = Modifier
            .align(Alignment.CenterVertically)
            .clickable { navController.navigate(NavRoutes.Month.route) }
            .semantics { testTagsAsResourceId = true }
            .testTag("BackRow")) {
            Icon(
                painter = painterResource(id = R.drawable.baseline_arrow_back_ios_24),
                contentDescription = stringResource(R.string.back)
            )
            Text(text = stringResource(R.string.back))
        }
        IconButton(onClick = {
            eventViewModel.setEvent(Event("", "", dayViewModel.getCalendarDateStringDb(), "", "", "", ""))
            navController.navigate(NavRoutes.Add.route + "/day")
                             },
            enabled = isNotPastDate(selectedDate),
            modifier = Modifier
                .semantics { testTagsAsResourceId = true }
                .testTag("AddEventButton")) {
            Icon(
                painter = painterResource(id = R.drawable.baseline_add_circle_24),
                contentDescription = stringResource(R.string.month_add_event),
                modifier = Modifier
                    .semantics { testTagsAsResourceId = true }
                    .testTag("AddEvent")
            )
        }
    }
}

@Composable
fun DisplayHolidays(dayViewModel : DayViewModel){
    val holidays = dayViewModel.dailyHolidays
    Row() {
        for(holiday in holidays){
            Text(text = holiday.localName,
                Modifier.background(shape = Shapes(
                    medium = RoundedCornerShape(8.dp),
                ).medium, color = Color.Blue)
                    .padding(3.dp),
                color = Color.White
            )
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DailySchedule(navController: NavController, dayViewModel: DayViewModel, eventViewModel: EventViewModel){
    val events = dayViewModel.events.observeAsState(listOf()).value
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
            .semantics { testTagsAsResourceId = true }
            .testTag("ScheduleRow")
    ) {
        Column(
            horizontalAlignment = Alignment.End,
            verticalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier
                .fillMaxHeight()
                .semantics { testTagsAsResourceId = true }
                .testTag("TimeDisplayColumn")
        ) {
            for (i in 0..24){
                Divider (
                    color = Color.Black,
                    modifier = Modifier
                        .height(1.dp)
                        .width(40.dp)
                )
                Text(
                    modifier = Modifier.height(40.dp),
                    text = dayViewModel.parseTime(i.toDouble())
                )
            }
        }
        Column(
            modifier = Modifier
                .semantics { testTagsAsResourceId = true }
                .testTag("EventsColumn")
        ) {
            var timeIndex = 0.0
            for(event in events){
                Spacer(modifier = Modifier.height(((processTime(event.startTime) * 41.2).dp)))
                DisplayEvent(navController, event, eventViewModel)
                timeIndex = processTime(event.endTime)
            }
        }
    }
}

fun processTime(timeString: String): Double {
    val timeWhole = timeString.split(":")[0].toDouble()
    val timeFraction = (timeString.split(":")[1].toDouble()) / 60.0
    return (timeWhole + timeFraction)
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun DisplayEvent(navController: NavController, event : DayEvent, eventViewModel : EventViewModel){
    Button(
        shape = Shapes(
            medium = RoundedCornerShape(8.dp),
        ).medium,
        modifier = Modifier
            .fillMaxWidth(0.95f)
            .height((event.duration * 41.2).dp)
            .semantics { testTagsAsResourceId = true }
            .testTag("displayEvent"),
        onClick = {
            eventViewModel.setEvent(event.event)
            navController.navigate(NavRoutes.View.route)
        }
    ) {
        Text(text = "${event.title} : ${event.startTime} - ${event.endTime}")
    }
}

fun isNotPastDate(selectedDate:String): Boolean{
    val calendar: Calendar = Calendar.getInstance()
    val month: String = (calendar.get(Calendar.MONTH) + 1).toString().padStart(2, '0')
    val day: String = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(2, '0')
    val year: String = calendar.get(Calendar.YEAR).toString()
    val currentDateString = "${year}/${month}/${day}"
    return selectedDate >= currentDateString
}