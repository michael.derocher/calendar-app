package com.example.calendarproject.month

import android.app.Application
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme
import com.example.calendarproject.R
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MonthViewModelTest {
    private var navController : NavHostController? = null

    private var viewModel : MonthViewModel? = null

    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent{
            CalendarProjectTheme {
                navController = rememberNavController()
                val context = LocalContext.current
                val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
                viewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings)
            }

        }
    }
    @Test
    fun monthViewModel_navigateToNextMonth_DisplayDaysForNextMonth()  {
        viewModel?.nextMonth()
        assertEquals("January 2024", viewModel?.currentMonthScreenDate)
        assertEquals(31, viewModel?.daysInCurrentMonth)
    }

    @Test
    fun monthViewModel_navigateToPreviousMonth_DisplayDaysForPreviousMonth()  {
        viewModel?.previousMonth()
        assertEquals("November 2023", viewModel?.currentMonthScreenDate)
        assertEquals(30, viewModel?.daysInCurrentMonth)

        viewModel?.previousMonth()
        assertEquals("October 2023", viewModel?.currentMonthScreenDate)
        assertEquals(31, viewModel?.daysInCurrentMonth)
    }

    @Test
    fun monthViewModel_getStartingDayOfWeek()  {
        viewModel?.getStartingDayOfWeek()
        assertEquals(5, viewModel?.getStartingDayOfWeek())

        viewModel?.previousMonth()

        viewModel?.getStartingDayOfWeek()
        assertEquals(3, viewModel?.getStartingDayOfWeek())

        viewModel?.previousMonth()

        viewModel?.getStartingDayOfWeek()
        assertEquals(0, viewModel?.getStartingDayOfWeek())
    }
}

