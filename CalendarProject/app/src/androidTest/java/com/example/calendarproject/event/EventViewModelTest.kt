package com.example.calendarproject.event

import android.app.Application
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.Calendar

@RunWith(AndroidJUnit4::class)
class EventViewModelTest {
    private var navController : NavHostController? = null

    private var viewModel : EventViewModel? = null

    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent{
            CalendarProjectTheme {
                navController = rememberNavController()
                viewModel = EventViewModel(LocalContext.current.applicationContext as Application)
            }
        }
    }

    @Test
    fun eventViewModel_onTitleChange() {
        viewModel?.onTitleChange("1")
        assertEquals(viewModel?.title, "1")
    }
    @Test
    fun eventViewModel_onSubjectChange() {
        viewModel?.onSubjectChange("2")
        assertEquals(viewModel?.subject, "2")
    }
    @Test
    fun eventViewModel_onDateChange() {
        viewModel?.onDateChange("3")
        assertEquals(viewModel?.date, "3")
    }
    @Test
    fun eventViewModel_onStartTimeChange() {
        viewModel?.onStartTimeChange("1:00")
        assertEquals(viewModel?.startTime, "1:00")
    }
    @Test
    fun eventViewModel_onEndTimeChange() {
        viewModel?.onEndTimeChange("23:00")
        assertEquals(viewModel?.endTime, "23:00")
    }
    @Test
    fun eventViewModel_onLocationChange() {
        viewModel?.onLocationChange("6")
        assertEquals(viewModel?.location, "6")
    }
    @Test
    fun eventViewModel_onDescriptionChange() {
        viewModel?.onDescriptionChange("7")
        assertEquals(viewModel?.description, "7")
    }

    @Test
    fun eventViewModel_defaultEvent_setsDefaultValuesToFields()  {
        val calendar = Calendar.getInstance()
        val month: String = (calendar.get(Calendar.MONTH) + 1).toString()
        val day: String = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(2, '0')
        val year: String = calendar.get(Calendar.YEAR).toString()

        assertEquals(viewModel?.title, "")
        assertEquals(viewModel?.subject, "")
        assertEquals(viewModel?.date, "${month}/${day}/${year}")
        assertEquals(viewModel?.startTime, "10:00")
        assertEquals(viewModel?.endTime, "11:00")
        assertEquals(viewModel?.location, "")
        assertEquals(viewModel?.description, "")
    }

    @Test
    fun eventViewModel_customEvent_setsDefaultValuesToFields()  {
        val event = Event("title1", "subject1", "date1", "1:00", "23:00", "location1", "description1")
        viewModel?.setEvent(event)
        assertEquals(viewModel?.title, "title1")
        assertEquals(viewModel?.subject, "subject1")
        assertEquals(viewModel?.date, "date1")
        assertEquals(viewModel?.startTime, "1:00")
        assertEquals(viewModel?.endTime, "23:00")
        assertEquals(viewModel?.location, "location1")
        assertEquals(viewModel?.description, "description1")
    }

}