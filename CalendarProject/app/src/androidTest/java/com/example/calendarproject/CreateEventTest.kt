package com.example.calendarproject

import android.app.Application
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertAll
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.isEnabled
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithTag
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performScrollTo
import androidx.navigation.compose.rememberNavController
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.screens.Event.CreateDecision
import com.example.calendarproject.Presentation.screens.Event.ManipulateEvent
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.Calendar

class CreateEventTest {

    private val testEvent = Event("test", "test", "5/5/2023", "10:30", "11:30", "test", "test")

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent {
            CalendarProjectTheme {
                val context = LocalContext.current
                val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
                val monthViewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings)
                val eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application)
                eventViewModel.setEvent(testEvent)
                val navController = rememberNavController()
                val calendar = Calendar.getInstance()
                ManipulateEvent(navController, context, calendar, { navController, viewModel -> CreateDecision(navController, viewModel, true, onErrorChange = {}) }, eventViewModel, monthViewModel,false, "Create")
            }
        }
    }

    // Note that Create Event and Edit Event extend the same common base composable. So most of the tests in
    // Create and Edit are shared. The shared ones are in the CreateEventTest.kt

    // This test covers both CreateEvent() and EditEvent() composables
    @Test
    fun manipulateEventTest() {
        composeTestRule.onNodeWithTag("topColumn").assertIsDisplayed()
    }

    @Test
    fun titleTest(){
        composeTestRule.onNodeWithText("Create Event").assertIsDisplayed()
    }

    @Test
    fun generalFieldTest(){
        composeTestRule.onNodeWithText("Title:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Subject:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Location:").assertIsDisplayed()
    }

    @Test
    fun dateTimeFieldTest(){
        composeTestRule.onNodeWithText("Start Time:").assertIsDisplayed()
        composeTestRule.onNodeWithText("End Time:").assertIsDisplayed()
    }

    @Test
    fun dateSelectionTest(){
        composeTestRule.onNodeWithTag("dateColumn").assertExists()
    }

    @Test
    fun timeSelectionTest(){
        composeTestRule.onAllNodesWithTag("timeColumn").assertAll(isEnabled())
    }

    @Test
    fun descriptionFieldTest(){
        composeTestRule.onNodeWithText("Description:").assertIsDisplayed()
    }

    @Test
    fun createDecisionTest(){
        composeTestRule.onNodeWithText("Create").assertIsDisplayed()
        composeTestRule.onNodeWithText("Create").performScrollTo()
        composeTestRule.onNodeWithText("Cancel").assertIsDisplayed()
    }
}