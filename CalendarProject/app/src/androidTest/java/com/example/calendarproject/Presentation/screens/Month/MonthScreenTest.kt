package com.example.calendarproject.Presentation.screens.Month

import android.app.Application
import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertAll
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.assertHasClickAction
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasClickAction
import androidx.compose.ui.test.hasNoClickAction
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onAllNodesWithTag
import androidx.compose.ui.test.onChild
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithTag
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.MainActivity
import com.example.calendarproject.Presentation.NavigationHost
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import com.example.calendarproject.R
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class MonthScreenTest {

    private var navController : NavHostController? = null

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUP() {
        composeTestRule.activity.setContent{
            navController = rememberNavController()
            val context = LocalContext.current
            val dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
            val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
            NavigationHost(navController = navController!!, monthViewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings),
                eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application),
                dayViewModel = DayViewModel(LocalDate.now(), LocalContext.current.applicationContext as Application, dayVMStrings),
                weatherViewModel = WeatherViewModel()
            )
            navController!!.navigate(NavRoutes.Month.route)
        }
    }

    @Test
    fun month() {
        composeTestRule.onNodeWithTag("MonthScaffold").assertIsDisplayed()
    }

    @Test
    fun title() {
        composeTestRule.onNodeWithTag("Title").assertIsDisplayed()
    }

    @Test
    fun previousMonthButton() {
        composeTestRule.onNodeWithTag("Previous").assertIsDisplayed().assertHasClickAction()
    }

    @Test
    fun nextMonthButton() {
        composeTestRule.onNodeWithTag("Next").assertIsDisplayed().assertHasClickAction()
    }

    @Test
    fun content() {
        composeTestRule.onNodeWithTag("ContentColumn").assertIsDisplayed()
    }

    @Test
    fun addEventButton() {
        composeTestRule.onNodeWithTag("AddEventRow").assertIsDisplayed().onChild().assertHasClickAction()
    }

    @Test
    fun daysOfTheWeek() {
        val daysOfWeekRow = composeTestRule.onNodeWithTag("DaysOfWeekRow")
        daysOfWeekRow.assertIsDisplayed()
        daysOfWeekRow.onChildren().assertCountEquals(7)
    }

    @Test
    fun day() {
        composeTestRule.onAllNodesWithTag("Day").assertAll(hasNoClickAction())
    }

    @Test
    fun clickableDay() {
        composeTestRule.onAllNodesWithTag("ClickableDay").assertAll(hasClickAction())
    }

    @Test
    fun daysOfMonth() {
        composeTestRule.onNodeWithTag("DaysOfMonthGrid").assertIsDisplayed()
    }
}