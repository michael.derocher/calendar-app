package com.example.calendarproject

import android.app.Application
import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.NavHostController
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.compose.ui.test.performScrollTo
import androidx.navigation.compose.rememberNavController
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.MainActivity
import com.example.calendarproject.Presentation.NavigationHost
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class CreateEventNavigationTest {
    private var navController : NavHostController? = null

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUP() {
        composeTestRule.activity.setContent{
            navController = rememberNavController()
            val context = LocalContext.current
            val dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
            val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
            NavigationHost(navController = navController!!, monthViewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings),
                eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application),
                dayViewModel = DayViewModel(LocalDate.now(), LocalContext.current.applicationContext as Application, dayVMStrings),
                weatherViewModel = WeatherViewModel()

            )
            navController!!.navigate(NavRoutes.Add.route + "/day")
        }
    }

    @Test
    fun testCurrentRoute() {
        composeTestRule.onNodeWithText("Create").assertIsDisplayed()
        val route = navController?.currentBackStackEntry?.destination?.route
        assertEquals(NavRoutes.Add.route + "/{fromView}", route)
    }

    @Test
    fun testCreateToView(){
        composeTestRule.onNodeWithText("Create").performScrollTo()
        composeTestRule.onNodeWithText("Create").assertIsDisplayed()
        composeTestRule.onNodeWithText("Create").performClick()

        val route = navController?.currentDestination?.route
        var correctRoute = false
        if (route == NavRoutes.View.route || route == NavRoutes.Add.route + "/{fromView}"){
            correctRoute = true
        }
        assertTrue(correctRoute)
    }

    @Test
    fun testCreateToMonth() {
        composeTestRule.onNodeWithText("Cancel").performScrollTo()
        composeTestRule.onNodeWithText("Cancel").assertIsDisplayed()
        composeTestRule.onNodeWithText("Cancel").performClick()
        val route = navController?.currentDestination?.route
        assertEquals(NavRoutes.Day.route + "/{fromView}", route)
    }

}