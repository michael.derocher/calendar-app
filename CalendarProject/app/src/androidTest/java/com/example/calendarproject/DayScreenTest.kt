package com.example.calendarproject

import android.app.Application
import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertAll
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasClickAction
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onAllNodesWithTag
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.onParent
import androidx.compose.ui.test.performClick
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.MainActivity
import com.example.calendarproject.Presentation.NavigationHost
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import org.junit.After

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class DayScreenKtTest {

    private var navController : NavHostController? = null
    private var eventViewModel : EventViewModel? = null
    private var dayViewModel : DayViewModel? = null

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUP() {
        composeTestRule.activity.setContent{
            navController = rememberNavController()
            val context = LocalContext.current
            val dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
            val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
            eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application)
            dayViewModel = DayViewModel(LocalDate.of(2023, 12, 25), LocalContext.current.applicationContext as Application, dayVMStrings)
            eventViewModel?.onTitleChange("Title 1")
            eventViewModel?.onSubjectChange("Subject 1")
            eventViewModel?.onDateChange("12/25/2023")
            eventViewModel?.onStartTimeChange("8:00")
            eventViewModel?.onEndTimeChange("9:00")
            eventViewModel?.onLocationChange("Location 1")
            eventViewModel?.onDescriptionChange("Description")
            eventViewModel?.createEvent()
            dayViewModel?.country_code = "CA"
            NavigationHost(navController = navController!!, monthViewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings),
                eventViewModel!!,
                dayViewModel = dayViewModel!!,
                weatherViewModel = WeatherViewModel()
            )
            navController!!.navigate(NavRoutes.Day.route + "/month")
        }
    }

    @After
    fun teardown() {
        for (event in dayViewModel?.events?.value!!){
            eventViewModel?.deleteEvent(event.event.id)
        }
    }

    @Test
    fun dailyOverviewScreen() {
        composeTestRule.onNodeWithTag("MainColumn").assertIsDisplayed()
    }

    @Test
    fun dateNavigator(){
        composeTestRule.onNodeWithTag("NavRow").assertIsDisplayed()
        composeTestRule.onNodeWithTag("PreviousDay", useUnmergedTree = true).assertIsDisplayed().onParent().performClick()
        composeTestRule.onNodeWithTag("NextDay", useUnmergedTree = true).assertIsDisplayed().onParent().performClick()
        composeTestRule.onNodeWithTag("DateTextDisplay").assertIsDisplayed()
    }

    @Test
    fun navigationBar_display(){
        composeTestRule.onNodeWithTag("NavbarRow").assertIsDisplayed()
        composeTestRule.onNodeWithTag("BackRow").assertIsDisplayed()
        composeTestRule.onNodeWithText("Back").assertIsDisplayed()
        composeTestRule.onNodeWithTag("AddEventButton").assertIsDisplayed()
        composeTestRule.onNodeWithTag("AddEvent", useUnmergedTree = true).assertIsDisplayed()
    }

    @Test
    fun navigationBar_navigate_back(){
        composeTestRule.onNodeWithTag("BackRow").assertIsDisplayed().performClick()
        val route = navController?.currentDestination?.route
        assertEquals(NavRoutes.Month.route, route)
    }

    @Test
    fun navigationBar_create_event(){
        composeTestRule.onNodeWithTag("AddEventButton").assertIsDisplayed().performClick()
        val route = navController?.currentDestination?.route
        assertEquals(NavRoutes.Add.route + "/{fromView}", route)
    }

    @Test
    fun dailySchedule(){
        composeTestRule.onNodeWithTag("ScheduleRow").assertIsDisplayed()
        val timeDisplayColumn = composeTestRule.onNodeWithTag("TimeDisplayColumn")
        timeDisplayColumn.assertIsDisplayed()
        timeDisplayColumn.onChildren().assertCountEquals(25)
        composeTestRule.onNodeWithTag("EventsColumn").assertIsDisplayed()
    }

    @Test
    fun displayEvent() {
        composeTestRule.onAllNodesWithTag("displayEvent").assertAll(hasClickAction())
        composeTestRule.onAllNodesWithTag("displayEvent")[0].performClick()
        val route = navController?.currentDestination?.route
        assertEquals(NavRoutes.View.route, route)
    }
}