package com.example.calendarproject.data

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Data.EventDao
import com.example.calendarproject.Data.EventRoomDatabase
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.After
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EventDaoTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: EventRoomDatabase
    private lateinit var eventDao: EventDao

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            EventRoomDatabase::class.java
        ).allowMainThreadQueries().build()

        eventDao = database.eventDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertEvent() = runBlocking {
        val event = Event("t", "s", "12/12/2023", "10:00", "11:00", "l", "d")

        eventDao.insertEvent(event)

        val results = eventDao.getAllEvents().getOrAwaitValue()

        assertEquals(1, results.size)
        assertEquals("t", results[0].title)
    }

    @Test
    fun getMonthEvents() = runBlocking {
        val event1 = Event("t1", "s", "12/06/2023", "10:00", "11:00", "l", "d")
        val event2 = Event("t2", "s", "11/07/2022", "10:00", "11:00", "l", "d")
        val event3 = Event("t3", "s", "11/05/2021", "10:00", "11:00", "l", "d")

        eventDao.insertEvent(event1)
        eventDao.insertEvent(event2)
        eventDao.insertEvent(event3)

        val results = eventDao.getMonthEvents("11", "2021")

        assertEquals(1, results.size)
        assertEquals("t3", results[0].title)
    }

    @Test
    fun getDateEvents() = runBlocking {
        val event1 = Event("t1", "s", "12/06/2023", "10:00", "11:00", "l", "d")
        val event2 = Event("t2", "s", "11/07/2022", "10:00", "11:00", "l", "d")
        val event3 = Event("t3", "s", "11/05/2021", "10:00", "11:00", "l", "d")

        eventDao.insertEvent(event1)
        eventDao.insertEvent(event2)
        eventDao.insertEvent(event3)

        val results = eventDao.getDateEventsLive("11/07/2022").getOrAwaitValue()

        assertEquals(1, results.size)
        assertEquals("t2", results[0].title)
    }

    @Test
    fun getEvent() = runBlocking {
        val event1 = Event("t1", "s", "12/06/2023", "10:00", "11:00", "l", "d")
        val event2 = Event("t2", "s", "11/07/2022", "10:00", "11:00", "l", "d")
        val event3 = Event("t3", "s", "11/05/2021", "10:00", "11:00", "l", "d")

        eventDao.insertEvent(event1)
        eventDao.insertEvent(event2)
        eventDao.insertEvent(event3)

        val result = eventDao.getEvent(2).getOrAwaitValue()

        assertNotEquals(null, result)
        assertEquals("t2", result.title)
    }

    @Test
    fun deleteEvent() = runBlocking {
        val event1 = Event("t1", "s", "12/06/2023", "10:00", "11:00", "l", "d")
        val event2 = Event("t2", "s", "11/07/2022", "10:00", "11:00", "l", "d")
        val event3 = Event("t3", "s", "11/05/2021", "10:00", "11:00", "l", "d")

        eventDao.insertEvent(event1)
        eventDao.insertEvent(event2)
        eventDao.insertEvent(event3)

        eventDao.deleteEvent(2)
        val results = eventDao.getAllEvents().getOrAwaitValue()

        assertEquals(2, results.size)
        assertEquals("t1", results[0].title)
        assertEquals("t3", results[1].title)
    }

    @Test
    fun updateEvent() = runBlocking {
        val event1 = Event("t", "s", "12/06/2023", "10:00", "11:00", "l", "d")
        val event2 = Event("t2", "s", "12/06/2023", "10:00", "11:00", "l", "d")

        eventDao.insertEvent(event1)
        eventDao.insertEvent(event2)
        eventDao.updateEvent("t1", "s1", "11/07/2022", "12:00", "13:00", "l1", "d1", 2)

        val results = eventDao.getAllEvents().getOrAwaitValue()

        assertEquals(2, results.size)
        assertEquals(2, results[1].id)
        assertEquals("t1", results[1].title)
        assertEquals("s1", results[1].subject)
        assertEquals("11/07/2022", results[1].date)
        assertEquals("12:00", results[1].startTime)
        assertEquals("13:00", results[1].endTime)
        assertEquals("l1", results[1].location)
        assertEquals("d1", results[1].description)
    }
}
