package com.example.calendarproject

import android.app.Application
import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.Condition
import com.example.calendarproject.Domain.Classes.Current
import com.example.calendarproject.Domain.Classes.DailyForecast
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Domain.Classes.ForecastDay
import com.example.calendarproject.Domain.Classes.FutureForecast
import com.example.calendarproject.Domain.Classes.HourlyForecast
import com.example.calendarproject.Domain.Classes.WeatherData
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.MainActivity
import com.example.calendarproject.Presentation.NavigationHost
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class WeatherNavigationTest {
    private var navController : NavHostController? = null

    @get: Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUP() {
        composeTestRule.activity.setContent{
            navController = rememberNavController()
            val weatherViewModel = WeatherViewModel()

            weatherViewModel.latitude.value = 1.0
            weatherViewModel.longitude.value = 1.0

            val current = Current("test", 0f, Condition("test", "test"), 0f, 0f, 0f, 0f)
            val futureForecasts = ArrayList<FutureForecast>(1)

            for (i in 0..4) {
                val dailyForecast = DailyForecast(i.toFloat(), i.toFloat(), Condition("test", "test"))
                val hourlyForecasts = ArrayList<HourlyForecast>(1)

                for (j in 0..23) {
                    hourlyForecasts.add(HourlyForecast(i.toFloat(), Condition("test", "test"), i.toFloat(), i.toFloat(), i.toFloat()))
                }

                futureForecasts.add(FutureForecast(dailyForecast, hourlyForecasts))
            }

            val forecastDay = ForecastDay(futureForecasts)

            weatherViewModel.data.value = WeatherData(current, forecastDay)

            val context = LocalContext.current
            val dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
            val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
            val eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application)
            eventViewModel.addEvent(Event("Mobile", "project", "11/13/2024", "13:00", "14:00", "College", "Very long project"))
            NavigationHost(navController = navController!!, monthViewModel = MonthViewModel(
                LocalContext.current.applicationContext as Application, monthVMStrings),
                eventViewModel,
                dayViewModel = DayViewModel(LocalDate.now(), LocalContext.current.applicationContext as Application, dayVMStrings),
                weatherViewModel = weatherViewModel
            )
            val testEvent = Event("Mobile", "project2", "11/13/2024", "18:00", "19:00", "College", "Very short project")
            eventViewModel.setEvent(testEvent)
            navController!!.navigate(NavRoutes.Weather.route)
        }
    }

    @Test
    fun testCurrentRoute() {
        composeTestRule.onNodeWithTag("back_button").assertIsDisplayed()
        val route = navController?.currentBackStackEntry?.destination?.route
        Assert.assertEquals(NavRoutes.Weather.route, route)
    }

    @Test
    fun testBackToDayView() {
        composeTestRule.onNodeWithTag("back_button").assertIsDisplayed()
        composeTestRule.onNodeWithTag("back_button").performClick()
        val route = navController?.currentBackStackEntry?.destination?.route
        Assert.assertEquals(NavRoutes.Day.route + "/{fromView}", route)
    }
    
}