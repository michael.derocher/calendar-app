package com.example.calendarproject

import android.app.Application
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performScrollTo
import androidx.navigation.compose.rememberNavController
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.screens.Event.EditDecision
import com.example.calendarproject.Presentation.screens.Event.ManipulateEvent
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme

import org.junit.Before
import org.junit.Rule
import org.junit.Test

import java.util.Calendar

class EditEventTest {
    private val testEvent = Event("test", "test", "test", "10:30", "11:30", "test", "test")

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent {
            CalendarProjectTheme {
                val context = LocalContext.current
                val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
                val monthViewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings)
                val eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application)
                eventViewModel.setEvent(testEvent)
                val navController = rememberNavController()
                val calendar = Calendar.getInstance()
                ManipulateEvent(navController, context, calendar, { viewModel, navController -> EditDecision(viewModel, navController, onErrorChange = {}) }, eventViewModel, monthViewModel, false, "Edit")
            }
        }
    }

    // Note that Create Event and Edit Event extend the same common base composable. So most of the tests in
    // Create and Edit are shared. The shared ones are in the CreateEventTest.kt

    @Test
    fun titleTest(){
        composeTestRule.onNodeWithText("Edit Event").assertIsDisplayed()
    }

    @Test
    fun editDecisionTest(){
        composeTestRule.onNodeWithText("Edit").assertIsDisplayed()
        composeTestRule.onNodeWithText("Edit").performScrollTo()
        composeTestRule.onNodeWithText("Cancel").assertIsDisplayed()
    }
}