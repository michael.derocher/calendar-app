package com.example.calendarproject

import android.app.Application
import androidx.activity.compose.setContent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollTo
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Domain.NavRoutes
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.MainActivity
import com.example.calendarproject.Presentation.NavigationHost
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.MonthViewModel
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel

import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class MonthNavigationTest {
    private val testEvent = Event("test", "test", "test", "10:00", "11:00", "test", "test")
    private var navController : NavHostController? = null

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUP() {
        composeTestRule.activity.setContent{
            val context = LocalContext.current
            val dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
            val monthVMStrings: List<String> = context.resources.getStringArray(R.array.monthVM).asList()
            val eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application)
            eventViewModel.setEvent(testEvent)
            navController = rememberNavController()
            NavigationHost(navController = navController!!, monthViewModel = MonthViewModel(LocalContext.current.applicationContext as Application, monthVMStrings),
                eventViewModel,
                dayViewModel = DayViewModel(LocalDate.now(), LocalContext.current.applicationContext as Application, dayVMStrings), weatherViewModel = WeatherViewModel()
            )
            navController!!.navigate(NavRoutes.Month.route)
        }
    }

    @Test
    fun testCurrentRoute() {
        composeTestRule.onNodeWithText("1").assertIsDisplayed()
        val route = navController?.currentBackStackEntry?.destination?.route
        Assert.assertEquals(NavRoutes.Month.route, route)
    }

    @Test
    fun testMonthToAddByAdd(){
        composeTestRule.onNodeWithTag("addEventButton").assertIsDisplayed()
        composeTestRule.onNodeWithTag("addEventButton").performClick()
        val route = navController?.currentDestination?.route
        Assert.assertEquals(NavRoutes.Add.route + "/{fromView}", route)
    }

    @Test
    fun testMonthToDayByDayClick() {
        composeTestRule.onNodeWithText("1").assertIsDisplayed()
        composeTestRule.onNodeWithText("1").performScrollTo()
        composeTestRule.onNodeWithText("1").performClick()
        val route = navController?.currentDestination?.route
        Assert.assertEquals(NavRoutes.Day.route + "/{fromView}", route)
    }
}