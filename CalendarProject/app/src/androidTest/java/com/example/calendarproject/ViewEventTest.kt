package com.example.calendarproject

import android.app.Application
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollTo
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.Event
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ViewModels.EventViewModel
import com.example.calendarproject.Presentation.screens.Event.ViewEvent
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Before
import org.junit.Rule
import java.time.LocalDate

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ViewEventKtTest {
    private val testEvent = Event("test", "test", "test", "10:00", "11:00", "test", "test")

    //Establish a Compose rule to get access to the composable component
    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent {
            CalendarProjectTheme {
                val context = LocalContext.current
                val dayVMStrings: Array<String> = context.resources.getStringArray(R.array.dayVM)
                val eventViewModel = EventViewModel(LocalContext.current.applicationContext as Application)
                eventViewModel.setEvent(testEvent)
                val navController = rememberNavController()
                ViewEvent(navController, eventViewModel, dayViewModel = DayViewModel(LocalDate.now(), LocalContext.current.applicationContext as Application, dayVMStrings))
            }
        }
    }

    @Test
    fun viewEventTest() {
        composeTestRule.onNodeWithText("< Back").assertIsDisplayed()
    }

    @Test
    fun titleTest(){
        composeTestRule.onNodeWithText("Event: test").assertIsDisplayed()
    }

    @Test
    fun lineTest(){
        composeTestRule.onNodeWithText("Subject:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Date:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Start Time:").assertIsDisplayed()
        composeTestRule.onNodeWithText("End Time:").assertIsDisplayed()
        composeTestRule.onNodeWithText("Location:").assertIsDisplayed()
    }

    @Test
    fun descriptionTest(){
        composeTestRule.onNodeWithText("Description:").performScrollTo()
        composeTestRule.onNodeWithText("Description:").assertIsDisplayed()
    }

    @Test
    fun optionLineTest(){
        composeTestRule.onNodeWithText("Edit").performScrollTo()
        composeTestRule.onNodeWithText("Edit").assertIsDisplayed()
        composeTestRule.onNodeWithText("Delete").performClick()
        composeTestRule.onNodeWithText("Are you sure you want to delete this event?").assertIsDisplayed()
    }
}
