package com.example.calendarproject

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.Condition
import com.example.calendarproject.Domain.Classes.Current
import com.example.calendarproject.Domain.Classes.DailyForecast
import com.example.calendarproject.Domain.Classes.ForecastDay
import com.example.calendarproject.Domain.Classes.FutureForecast
import com.example.calendarproject.Domain.Classes.HourlyForecast
import com.example.calendarproject.Domain.Classes.WeatherData
import com.example.calendarproject.Presentation.ViewModels.WeatherViewModel
import com.example.calendarproject.Presentation.screens.weather.WeatherForecastScreen
import com.example.calendarproject.Presentation.screens.weather.getNextDaysOfWeek
import com.example.calendarproject.Presentation.screens.weather.truncateStrings
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

@RunWith(AndroidJUnit4::class)
class WeatherScreenTest {
    private val weatherViewModel = WeatherViewModel()

    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent {
            CalendarProjectTheme {
                val navController = rememberNavController()

                weatherViewModel.latitude.value = 1.0
                weatherViewModel.longitude.value = 1.0

                val current = Current("test", 0f, Condition("test", "test"), 0f, 0f, 0f, 0f)
                val futureForecasts = ArrayList<FutureForecast>(1)

                for (i in 0..4) {
                    val dailyForecast = DailyForecast(i.toFloat(), i.toFloat(), Condition("test", "test"))
                    val hourlyForecasts = ArrayList<HourlyForecast>(1)

                    for (j in 0..23) {
                        hourlyForecasts.add(HourlyForecast(0f, Condition("test", "test"), 0f, 0f, 0f))
                    }

                    futureForecasts.add(FutureForecast(dailyForecast, hourlyForecasts))
                }

                val forecastDay = ForecastDay(futureForecasts)

                weatherViewModel.data.value = WeatherData(current, forecastDay)

                WeatherForecastScreen(weatherViewModel, navController)
            }
        }
    }

    @Test
    fun backToDayScreenButtonTest() {
        composeTestRule.onNodeWithTag("back_button").assertIsDisplayed()
    }

    @Test
    fun currentForecastTest() {
        composeTestRule.onNodeWithTag("weather").assertIsDisplayed()
        composeTestRule.onNodeWithTag("last_updated").assertIsDisplayed()
        composeTestRule.onNodeWithTag("now").assertIsDisplayed()
        composeTestRule.onNodeWithTag("current_temp").assertIsDisplayed()
        composeTestRule.onNodeWithTag("feels_like").assertIsDisplayed()
        composeTestRule.onNodeWithTag("current_description").assertIsDisplayed()
        composeTestRule.onNodeWithTag("current_precipitation").assertIsDisplayed()
        composeTestRule.onNodeWithTag("current_humidity").assertIsDisplayed()
        composeTestRule.onNodeWithTag("current_wind").assertIsDisplayed()
    }

    @Test
    fun tabsTest() {
        composeTestRule.onNodeWithTag("overview_tab").assertIsDisplayed()
        composeTestRule.onNodeWithTag("precipitation_tab").assertIsDisplayed()
        composeTestRule.onNodeWithTag("wind_tab").assertIsDisplayed()
        composeTestRule.onNodeWithTag("humidity_tab").assertIsDisplayed()
    }

    @Test
    fun hourlyForecastsTest() {
        composeTestRule.onNodeWithText("0:00").assertExists()
        composeTestRule.onNodeWithText("3:00").assertExists()
        composeTestRule.onNodeWithText("6:00").assertExists()
        composeTestRule.onNodeWithText("9:00").assertExists()
        composeTestRule.onNodeWithText("12:00").assertExists()
        composeTestRule.onNodeWithText("15:00").assertExists()
        composeTestRule.onNodeWithText("18:00").assertExists()
        composeTestRule.onNodeWithText("21:00").assertExists()
    }

    @Test
    fun dailyForecastsTest() {
        composeTestRule.onNodeWithText("0°/0°").assertExists()
        composeTestRule.onNodeWithText("1°/1°").assertExists()
        composeTestRule.onNodeWithText("2°/2°").assertExists()
        composeTestRule.onNodeWithText("3°/3°").assertExists()
        composeTestRule.onNodeWithText("4°/4°").assertExists()
    }

    @Test
    fun getNextDaysOfWeekTest() {
        assertEquals(
            listOf("SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY"),
            getNextDaysOfWeek(LocalDate.parse("2023-01-01"), 5)
        )
    }

    @Test
    fun testTruncateStrings() {
        assertEquals(
            listOf("abc", "123", "XYZ"),
            truncateStrings(listOf("abcdef", "12345", "XYZWXYZ"), 3)
        )
    }
}