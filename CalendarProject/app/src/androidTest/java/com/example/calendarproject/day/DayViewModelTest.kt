package com.example.calendarproject.day

import android.app.Application
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.calendarproject.Domain.Classes.HolidayResponse
import com.example.calendarproject.Presentation.ViewModels.DayViewModel
import com.example.calendarproject.Presentation.ui.theme.CalendarProjectTheme
import com.example.calendarproject.R
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate


@RunWith(AndroidJUnit4::class)
class DayViewModelTest {
    private var navController : NavHostController? = null

    private var viewModel: DayViewModel? = null

    @get: Rule
    val composeTestRule = createComposeRule()

    @Before
    fun setUP() {
        composeTestRule.setContent{
            CalendarProjectTheme {
                navController = rememberNavController()
                val context = LocalContext.current
                val dayVMStrings = context.resources.getStringArray(R.array.dayVM)
                val application = LocalContext.current.applicationContext as Application
                viewModel = DayViewModel(LocalDate.of(2023, 12, 31), application, dayVMStrings)
            }
        }
    }

    @Test
    fun fetchYearHolidays(){
        viewModel?.fetchYearHolidays("2023", "CA")

        val expected = listOf<HolidayResponse>(
            HolidayResponse("2023-01-01", "New Year's Day"),
            HolidayResponse("2023-02-20", "Louis Riel Day"),
            HolidayResponse("2023-02-20","Islander Day"),
            HolidayResponse("2023-02-20","Heritage Day"),
            HolidayResponse("2023-02-20","Family Day"),
            HolidayResponse("2023-03-17","Saint Patrick's Day"),
            HolidayResponse("2023-04-07","Good Friday"),
            HolidayResponse("2023-04-10","Easter Monday"),
            HolidayResponse("2023-04-23","Saint George's Day"),
            HolidayResponse("2023-05-22","National Patriots' Day"),
            HolidayResponse("2023-05-22","Victoria Day"),
            HolidayResponse("2023-06-21","National Aboriginal Day"),
            HolidayResponse("2023-06-24","Discovery Day"),
            HolidayResponse("2023-06-24","Fête nationale du Québec"),
            HolidayResponse("2023-07-01","Canada Day"),
            HolidayResponse("2023-07-12","Orangemen's Day"),
            HolidayResponse("2023-08-07","Civic Holiday"),
            HolidayResponse("2023-08-07","British Columbia Day"),
            HolidayResponse("2023-08-07","Heritage Day"),
            HolidayResponse("2023-08-07","New Brunswick Day"),
            HolidayResponse("2023-08-07","Natal Day"),
            HolidayResponse("2023-08-07","Saskatchewan Day"),
            HolidayResponse("2023-08-21","Gold Cup Parade Day"),
            HolidayResponse("2023-08-21","Discovery Day"),
            HolidayResponse("2023-09-04","Labour Day"),
            HolidayResponse("2023-09-30","National Day for Truth and Reconciliation"),
            HolidayResponse("2023-10-09","Thanksgiving"),
            HolidayResponse("2023-11-11","Armistice Day"),
            HolidayResponse("2023-11-11","Remembrance Day"),
            HolidayResponse("2023-12-25","Christmas Day"),
            HolidayResponse("2023-12-26","Boxing Day")
        )

        while (viewModel?.yearlyHolidays?.value == null){

        }
        val response = viewModel?.yearlyHolidays?.value
        for ((count, holiday) in expected.withIndex()){
            assertEquals(holiday.date, response?.get(count)?.date)
            assertEquals(holiday.localName, response?.get(count)?.localName)
        }

    }

    @Test
    fun updateDailyHolidays(){
        viewModel?.fetchYearHolidays("2023", "CA")
        viewModel?.setDate(LocalDate.of(2023, 12, 25))
        while (viewModel?.yearlyHolidays?.value == null){

        }

        val expected = listOf<HolidayResponse>(
            HolidayResponse("2023-12-25","Christmas Day")
        )
        viewModel?.updateDailyHolidays()

        for ((count, holiday) in viewModel?.dailyHolidays!!.withIndex()){
            assertEquals(expected[count].date, holiday.date)
            assertEquals(expected[count].localName, holiday.localName)
        }
    }

    @Test
    fun nextDay() {

        viewModel?.nextDay()

        assertEquals(1, viewModel?.currentDay)
        assertEquals("January", viewModel?.currentMonth)
        assertEquals(2024, viewModel?.currentYear)
        assertEquals("Monday", viewModel?.currentWeekday)
    }

    @Test
    fun previousDay() {

        viewModel?.previousDay()

        assertEquals(30, viewModel?.currentDay)
        assertEquals("December", viewModel?.currentMonth)
        assertEquals(2023, viewModel?.currentYear)
        assertEquals("Saturday", viewModel?.currentWeekday)
    }

    @Test
    fun parseTime() {

        val result = viewModel?.parseTime(13.5)

        assertEquals("13:30", result)
    }

    @Test fun displayDayText(){

        val result1 = viewModel?.displayDayText(1)
        val result2 = viewModel?.displayDayText(22)
        val result3 = viewModel?.displayDayText(23)
        val result4 = viewModel?.displayDayText(11)

        assertEquals("1st", result1)
        assertEquals("22nd", result2)
        assertEquals("23rd", result3)
        assertEquals("11th", result4)
    }
}